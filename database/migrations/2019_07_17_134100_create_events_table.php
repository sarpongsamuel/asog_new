<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->Increments('id');
            $table->string('name');
            $table->string('host');
            $table->date('event_date');
            $table->string('event_venue');
            $table->text('content');
            $table->string('photo');
            $table->integer('title_id')->unsigned()->index();
            $table->boolean('registration_ongoing')->default(0);
            //foreign key
            $table->foreign('title_id')->references('id')->on('titles')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
