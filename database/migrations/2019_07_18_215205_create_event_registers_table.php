<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventRegistersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_registers', function (Blueprint $table) {
            $table->Increments('id');
            $table->string('title');
            $table->string('last_name');
            $table->string('other_names');
            $table->string('institution');
            $table->string('email');
            $table->string('telephone_1');
            $table->string('telephone_2')->nullable();
            $table->string('address');
            $table->string('name_order');
            $table->string('status');
            $table->string('accommodation');
            $table->string('num_of_days_staying');
            $table->string('registration_code');
            $table->integer('event_id')->unsigned()->index();
            $table->foreign('event_id')->references('id')->on('events')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_registers');
    }
}
