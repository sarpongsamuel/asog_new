<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePublicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('publications', function (Blueprint $table) {
            $table->Increments('id');
            $table->string('title');
            $table->text('author');
            $table->string('publication_date');
            $table->string('document', 255);
            $table->integer('publication_category_id')->unsigned()->index();
            // $table->integer('assoc_school_id')->unsigned()->index();
            $table->timestamps();

            // $table->foreign('assoc_school_id')->references('id')->on('assoc_schools')->onDelete('cascade');
            $table->foreign('publication_category_id')->references('id')->on('publication_categories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('publications');
    }
}
