@extends('Layouts.dashboard')
@section('title', '| Create Slider')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">Add New Slider</div>
            <div class="card-body">
                <div class="card-title">
                    <h3 class="text-center title-2">New Slider</h3>
                </div>
                <hr>
                {!! Form::model($slider) !!}
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            {{ Form::label('header', 'Head Caption', ['class' => 'control-label mb-1']) }}
                            {{ Form::text('header', null, ['class' => 'form-control', 'disabled'=>'true'])}}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            {{ Form::label('context', 'Sub Caption', ['class' => 'control-label mb-1']) }} <br>
                            {{ Form::textarea('context', null, ['class' => 'form-control', 'disabled'=>'true', 'rows'=>'8'])}}
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('image', 'Slider Image', ['class' => 'control-label mb-1']) }} <br>
                            <img src="{{asset('images/slide/'.$slider->image)}}" alt="" class="img-responsive img-thumbnail"
                                style="height:200px">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <a href="{{route('slider.index')}}" class="btn btn-success form-control">Back To Sliders</a>
                    </div>
                </div>

                {{Form::close()}}
            </div>
        </div>
    </div>

</div>

@endsection