@extends('Layouts.dashboard')
@section('title', '| Create Slider')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">Add New Slider</div>
            <div class="card-body">
                <div class="card-title">
                    <h3 class="text-center title-2">New Slider</h3>
                </div>
                <hr>
                {!! Form::open(['route' => 'slider.store', 'data-parsley-validate' => '', 'files'=>true]) !!}

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            {{ Form::label('header', 'Head Caption', ['class' => 'control-label mb-1']) }}
                            {{ Form::text('header', null, ['class' => 'form-control'])}}
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            {{ Form::label('image', 'Slider Image', ['class' => 'control-label mb-1']) }} <br>
                            {{ Form::file('image', null, ['class' => 'form-control', 'required'=>''])}}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            {{ Form::label('context', 'Sub Caption', ['class' => 'control-label mb-1']) }} <br>
                            {{ Form::textarea('context', null, ['class' => 'form-control', 'rows'=>'5'])}}
                        </div>
                    </div>
                </div>


                {!! Form::submit('Add Image', array('class' => 'btn btn-success btn-block', 'id'=>'btnSubmit')) !!}
                {{Form::close()}}
            </div>
        </div>
    </div>

</div>

@endsection