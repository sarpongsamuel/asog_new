@extends('Layouts.dashboard')
@section('title', '| Home Slider')

@section('content')
<div class="col-lg-12">
    <!-- USER DATA-->
    @include('partials._message')

    <div class="user-data m-b-30">
        <h3 class="title-3 m-b-30">
            <i class="zmdi zmdi-account-calendar"></i><a href="{{route('slider.create')}}"
                class="btn btn-primary btn-right">Add Home Slider</a></h3>

        <div class="table-responsive table-data">
            <table class="table">
                <thead>
                    <tr>
                        <td>
                            <label class="au-checkbox">
                                <input type="checkbox">
                                <span class="au-checkmark"></span>
                            </label>
                        </td>
                        <td><b>Slide</b></td>
                        <td><b>Header</b></td>
                        <td><b>Caption</b></td>
                        <td></td>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($sliders as $slider)
                    <tr>
                        <td>
                            <label class="au-checkbox">
                                <input type="checkbox">
                                <span class="au-checkmark"></span>
                            </label>
                        </td>
                        <td>
                            <div class="table-data__info">
                                <h6><img src="{{ asset('images/slide/' . $slider->image) }}" alt="" style="height:70px"></h6>
                            </div>
                        </td>

                        <td>
                            <div class="table-data__info">
                                <h6>{{$slider->header}}</h6>
                            </div>
                        </td>
                        <td>
                            <div class="table-data__info">
                                <h6>{{ substr($slider->context, 0, 30)}}{{ strlen($slider->context )> 30 ?"...":"" }}
                                </h6>
                            </div>
                        </td>
                        <td>
                            <div class="table-data-feature">
                                {{Form::Open(['route'=>['slider.destroy',$slider->id], 'method'=>'delete'])}}
                                <button class="item" type="submit" data-toggle="tooltip" data-placement="top"
                                    title="Delete">
                                    <i class="zmdi zmdi-delete" style="color:red"></i>
                                </button>
                                {{Form::close()}}
                                <a href="{{route('slider.show',$slider->id)}}"><button class="item" type="button"
                                        data-toggle="tooltip" data-placement="top" title="view">
                                        <i class="zmdi zmdi-eye" style="color:green"></i>
                                    </button></a>
                                <a href="{{route('slider.edit',$slider->id)}}"><button class="item" type="button"
                                        data-toggle="tooltip" data-placement="top" title="edit">
                                        <i class="zmdi zmdi-edit" style="color:blue"></i>
                                    </button>
                            </div>
                        </td>
                    </tr>
                    @empty
                    @endforelse


                </tbody>
            </table>
        </div>

    </div>
    <!-- END USER DATA-->
</div>

@endsection