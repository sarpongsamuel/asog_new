@extends('Layouts.dashboard')
@section('title', '| Registration Code')

@section('content')
<div class="row">
    <div class="col-md-12">
        <!-- DATA TABLE -->
        <br>
        <h3 class="title-5 m-b-35">All Registration Codes</h3>
        <div class="table-data__tool">
            <div class="table-data__tool-left">
                <button class="au-btn-filter">
                    <i class="zmdi zmdi-filter-list"></i>filters</button>
            </div>
            @if ((Auth::user()->is_admin))
            <div class="table-data__tool-right">
                {{ Form::open(['route' => 'registration_code.store']) }}
                {{ Form::submit('Generate new codes', array('class' => 'btn-danger au-btn--small'))
                }}
                {{Form::close()}}
            </div>
            @endif

        </div>
        <div class="table-responsive table-responsive-data2">
            <table class="table table-data2" id="registration_code_table">
                <thead>
                    <tr>
                        <th>
                            <label class="au-checkbox">
                                <input type="checkbox">
                                <span class="au-checkmark"></span>
                            </label>
                        </th>
                        <th>Code</th>
                        <th>Assigned To</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @php
                    $index = 0
                    @endphp
                    @forelse ($codes as $code)
                    @php
                    $index ++
                    @endphp
                    <tr class="tr-shadow">
                        <td>
                            <label class="au-checkbox">
                                <input type="checkbox">
                                <span class="au-checkmark"></span>
                            </label>
                        </td>
                        <td><span class="block-email">{{$code->code}}</span></td>
                        <td>{{$code->phone}}</td>
                        <td>
                            <div class="table-data-feature">
                                <button class="item" data-toggle="modal" data-target="#exampleModal_{{$index}}"
                                    title="Edit">
                                    <a href="javascript:void(0)"><i class="zmdi zmdi-edit" style="color:green"></i></a>
                                </button>
                                {{Form::open(['route'=>['registration_code.destroy',$code->id], 'method'=>'delete'])}}
                                <button class="item" {{ $code->phone !== null ? 'disabled' :'' }} data-toggle="tooltip"
                                    data-placement="top" title="Delete">
                                    <i class="zmdi zmdi-delete" style="color:red"></i>
                                </button>
                                {{Form::close()}}

                            </div>
                        </td>
                    </tr>
                    <tr class="spacer"></tr>
                    <div class="modal fade" id="exampleModal_{{$index}}" tabindex="-1" role="dialog"
                        aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Assign To</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    {{ Form::open(['route' => ['registration_code.update', $code->id], 'data-parsley-validate' =>'', 'method' => 'PUT']) }}
                                    {{ Form::text('code', $code->code, ['class' => 'form-control input_box m-b-20', 'required'=>'', 'disabled' => ''])}}
                                    {{ Form::text('phone', null, ['class' => 'form-control input_box', 'placeholder' => 'phone number', 'required' => '', 'data-parsley-type' => "digits", 'autocomplete' => 'off'])}}
                                    <button type="submit" class="btn btn-success btn-block m-t-20">Assign</button>
                                    {{ Form::close() }}
                                </div>

                            </div>
                        </div>
                    </div>
                    @empty

                    @endforelse

                </tbody>
            </table>
        </div>
        <!-- END DATA TABLE -->
    </div>
</div>
@endsection

@push('scripts')
<script>
    $(() => {
        $('#search_input').keyup(($this) => {
            var value = $('#search_input').val().toLowerCase();
            $("#registration_code_table tr").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        })
    })
</script>
@endpush