@extends('Layouts.dashboard')
@section('title', '| Publication Category')

@section('content')
<div class="col-lg-12">
    <!-- USER DATA-->
    @include('partials._message')

    <div class="user-data m-b-30">
        <h3 class="title-3 m-b-30">
            <i class="zmdi zmdi-account-calendar"></i>Publication Category</h3>


        {!! Form::open(['route' => 'publicationCategory.store','data-parsley-validate' => '']) !!}
        <div class="form-group">
            <div class="row">
                <div class="col md-6 offset-2">
                    {{ Form::text('name', null, ['class' => 'form-control ', 'required'=>'', 'data-parsley-pattern' => "/^[a-zA-Z -\s]+$/", 'autocomplete' => 'off'])}}
                </div>
                <div class="col-md-3">
                    {{Form::submit('Add Publication Category',['class'=>'btn btn-primary'])}}
                </div>
            </div>
        </div>
        {!! Form::close() !!}

        <div class="table-responsive table-data">
            <table class="table">
                <thead>
                    <tr>
                        <td>
                            <label class="au-checkbox">
                                <input type="checkbox">
                                <span class="au-checkmark"></span>
                            </label>
                        </td>
                        <td><b>Publication Category</b></td>

                        <td></td>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($categories as $category)
                    <tr>
                        <td>
                            <label class="au-checkbox">
                                <input type="checkbox">
                                <span class="au-checkmark"></span>
                            </label>
                        </td>
                        <td>
                            <div class="table-data__info">
                                <h6>{{$category->name}}</h6>
                            </div>
                        </td>
                        <td>
                            <div class="table-data-feature">
                                {{Form::Open(['route'=>['publicationCategory.destroy',$category->id], 'method'=>'delete'])}}
                                <button class="item" type="submit" data-toggle="tooltip" data-placement="top"
                                    title="Delete">
                                    <i class="zmdi zmdi-delete" style="color:red"></i>
                                </button>
                                {{Form::close()}}
                            </div>
                        </td>
                    </tr>
                    @empty
                    @endforelse


                </tbody>
            </table>
        </div>

    </div>
    <!-- END USER DATA-->
</div>

@endsection