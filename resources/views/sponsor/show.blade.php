@extends('Layouts.dashboard')
@section('title', '|Gallery-Create')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">View Sponsor</div>
            <div class="card-body">
                <div class="card-title">
                    <h3 class="text-center title-2">View Sponsor</h3>
                </div>
                <hr>
                {!! Form::model($sponsor, ['route' => ['sponsor.update',$sponsor->id],'data-parsley-validate' => '',
                'files'=>true, 'method' => 'PUT']) !!}
                <div class="row">
                    <div class="col-md-5">
                        <div class="form-group">
                            {{ Form::label('name', 'Company Name', ['class' => 'control-label mb-1']) }}
                            {{ Form::text('name', null, ['class' => 'form-control', 'required'=>'','disabled'=>true])}}
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('location', 'Location', ['class' => 'control-label mb-1']) }} <br>
                            {{ Form::text('location', null, ['class' => 'form-control', 'required'=>'','disabled'=>true])}}
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            {{ Form::label('contact', 'Company Contact 1', ['class' => 'control-label mb-1']) }} <br>
                            {{ Form::number('contact1', null, ['class' => 'form-control', 'required'=>'','disabled'=>true])}}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            {{ Form::label('contact', 'Company Contact 2', ['class' => 'control-label mb-1']) }} <br>
                            {{ Form::number('contact2', null, ['class' => 'form-control', 'required'=>'','disabled'=>true])}}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {{ Form::label('details', 'Company Details', ['class' => 'control-label mb-1']) }} <br>
                            {{ Form::textarea('details', null, ['class' => 'form-control', 'required'=>'', 'rows'=>'10' ,'disabled'=>true])}}
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            {{ Form::label('logo', 'Company Logo', ['class' => 'control-label mb-1']) }} <br>
                            <img src="{{asset('images/sponsor/'.$sponsor->logo)}}" alt="" class="img-thumbnail"
                                style="height:200px">
                            {{-- {{ Form::file('logo', null, ['class' => 'form-control', 'required'=>'','disabled'=>true])}}
                            --}}
                        </div>
                    </div>
                </div>
                <a href="{{route('sponsor.index')}}" class="btn btn-danger btn-block">Cancel</a>

                {{-- {!! Form::submit('update Sponsor', array('class' => 'btn btn-success btn-block', 'id'=>'btnSubmit')) !!}  --}}
                {{Form::close()}}
            </div>
        </div>
    </div>

</div>

@endsection