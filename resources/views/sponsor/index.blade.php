@extends('Layouts.dashboard')
@section('title', '| Sponsors')

@section('content')
<div class="col-lg-12">
    <!-- USER DATA-->
    @include('partials._message')

    <div class="user-data m-b-30">
        <h3 class="title-3 m-b-30">
            <i class="zmdi zmdi-account-calendar"></i>
            <a href="{{route('sponsor.create')}}" class="btn btn-primary btn-right">Add New Sponsor</a>
            <a href="{{route('newsponsor.index')}}" class="btn btn-info btn-right">Sponsor Request</a>
        </h3>


        <div class="table-responsive table-data">
            <table class="table">
                <thead>
                    <tr>
                        <td>
                            <label class="au-checkbox">
                                <input type="checkbox">
                                <span class="au-checkmark"></span>
                            </label>
                        </td>
                        <td><b>Company Logo</b></td>
                        <td><b>Company Name</b></td>
                        <td><b>Location</b></td>
                        <td><b>Contact 1</b></td>
                        <td><b>Contact 2</b></td>
                        <td><b>Company Details</b></td>
                        <td></td>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($sponsors as $sponsor)
                    <tr>
                        <td>
                            <label class="au-checkbox">
                                <input type="checkbox">
                                <span class="au-checkmark"></span>
                            </label>
                        </td>
                        <td>
                            <div class="table-data__info">
                                <h6><img src="{{ asset('images/sponsor/' . $sponsor->logo) }}" alt="" style="height:70px"></h6>
                            </div>
                        </td>
                        <td>
                            <div class="table-data__info">
                                <h6>{{$sponsor->name}}</h6>
                            </div>
                        </td>
                        <td>
                            <div class="table-data__info">
                                <h6>{{$sponsor->location}}</h6>
                            </div>
                        </td>
                        <td>
                            <div class="table-data__info">
                                <h6>{{$sponsor->contact1}}</h6>
                            </div>
                        </td>
                        <td>
                            <div class="table-data__info">
                                <h6>{{$sponsor->contact2}}</h6>
                            </div>
                        </td>
                        <td>
                            <div class="table-data__info">
                                <h6>{{ substr($sponsor->details, 0, 100)}}{{ strlen($sponsor->details )> 100 ?"...":"" }}
                                </h6>
                            </div>
                        </td>
                        <td>
                            <div class="table-data-feature">
                                <a href="{{route('sponsor.show', $sponsor->id)}}"><button class="item" type="submit"
                                        data-toggle="tooltip" data-placement="top" title="view">
                                        <i class="zmdi zmdi-eye" style="color:blue"></i>
                                    </button><a>
                                        <a href="{{route('sponsor.edit', $sponsor->id)}}"><button class="item"
                                                type="submit" data-toggle="tooltip" data-placement="top" title="Edit">
                                                <i class="zmdi zmdi-edit" style="color:blue"></i>
                                            </button><a>
                                                {{Form::Open(['route'=>['sponsor.destroy',$sponsor->id], 'method'=>'delete'])}}
                                                <button class="item" type="submit" data-toggle="tooltip"
                                                    data-placement="top" title="Delete">
                                                    <i class="zmdi zmdi-delete" style="color:red"></i>
                                                </button>
                                                {{Form::close()}}
                            </div>
                        </td>
                    </tr>
                    @empty
                    @endforelse


                </tbody>
            </table>
        </div>

    </div>
    <!-- END USER DATA-->
</div>

@endsection