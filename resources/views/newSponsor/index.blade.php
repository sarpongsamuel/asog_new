@extends('Layouts.dashboard')
@section('title', '| Sponsors')

@section('content')
<div class="col-lg-12">
    <!-- USER DATA-->
    @include('partials._message')

    <div class="user-data m-b-30">
        <h3 class="title-3 m-b-30">
            <i class="zmdi zmdi-account-calendar">New Sponsor Request</i>
        </h3>


        <div class="table-responsive table-data">
            <table class="table">
                <thead>
                    <tr>
                        <td>
                            <label class="au-checkbox">
                                <input type="checkbox">
                                <span class="au-checkmark"></span>
                            </label>
                        </td>

                        <td><b>Company Name</b></td>
                        <td><b>Full Name</b></td>
                        <td><b>Email</b></td>
                        <td><b>Contact</b></td>
                        <td><b>Message</b></td>
                        <td></td>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($new as $sponsor)
                    <tr>
                        <td>
                            <label class="au-checkbox">
                                <input type="checkbox">
                                <span class="au-checkmark"></span>
                            </label>
                        </td>
                        <td>
                            <div class="table-data__info">
                                <h6>{{$sponsor->company}}</h6>
                            </div>
                        </td>
                        <td>
                            <div class="table-data__info">
                                <h6>{{$sponsor->fullname}}</h6>
                            </div>
                        </td>
                        <td>
                            <div class="table-data__info">
                                <h6>{{$sponsor->email}}</h6>
                            </div>
                        </td>
                        <td>
                            <div class="table-data__info">
                                <h6>{{$sponsor->telephone}}</h6>
                            </div>
                        </td>

                        <td>
                            <div class="table-data__info">
                                <h6>{{ substr($sponsor->message, 0, 100)}}{{ strlen($sponsor->message )> 100 ?"...":"" }}
                                </h6>
                            </div>
                        </td>
                        <td>
                            <div class="table-data-feature">
                                <a href="{{route('newsponsor.show', $sponsor->id)}}"><button class="item" type="submit"
                                        data-toggle="tooltip" data-placement="top" title="view">
                                        <i class="zmdi zmdi-eye" style="color:blue"></i>
                                    </button><a>

                                        {{Form::Open(['route'=>['newsponsor.destroy',$sponsor->id], 'method'=>'delete'])}}
                                        <button class="item" type="submit" data-toggle="tooltip" data-placement="top"
                                            title="Delete">
                                            <i class="zmdi zmdi-delete" style="color:red"></i>
                                        </button>
                                        {{Form::close()}}
                            </div>
                        </td>
                    </tr>
                    @empty
                    @endforelse


                </tbody>
            </table>
        </div>

    </div>
    <!-- END USER DATA-->
</div>

@endsection