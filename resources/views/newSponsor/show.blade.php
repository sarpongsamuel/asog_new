@extends('Layouts.dashboard')
@section('title', '| Event-Details')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">Add New Event</div>
            <div class="card-body">
                <div class="card-title">
                    <h3 class="text-center title-2">New Event</h3>
                </div>
                <hr>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">Event</div>
                            <div class="card-body">
                                <div class="card-title">
                                    <h3 class="text-center title-2"> Event</h3>
                                </div>
                                <hr>

                                {{ Form::model($new) }}
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            {{ Form::label('name', 'Full Name', ['class' => 'control-label mb-1']) }}
                                            {{ Form::text('fullname', null, ['class' => 'form-control', 'disabled'=>'true'])}}
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            {{ Form::label('Company', 'Company', ['class' => 'control-label mb-1']) }}
                                            {{ Form::text('company', null, ['class' => 'form-control', 'disabled'=>'true'])}}
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            {{ Form::label('email', 'email', ['class' => 'control-label mb-1']) }}
                                            {{ Form::text('email', null, ['class' => 'form-control', 'disabled'=>'true'])}}
                                        </div>
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            {{ Form::label('contact', 'contact', ['class' => 'control-label mb-1']) }}
                                            {{ Form::text('telephone', null, ['class' => 'form-control', 'disabled'=>'true'])}}
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            {{ Form::label('content', 'Event Content', ['class' => 'control-label mb-1']) }}
                                            {{ Form::textarea('message', null, ['class' => 'form-control', 'disabled'=>'true'])}}
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <a href="{{route('sponsor.index')}}" class="btn btn-success btn-block">Ok</a>
                                    </div>
                                </div>
                                {{Form::close()}}
                            </div>
                        </div>
                    </div>

                </div>



                @endsection