@extends('Layouts.dashboard')
@section('title', '| Training & Mentorship')

@section('content')
<div class="col-lg-12">
    <!-- USER DATA-->
    @include('partials._message')

    <div class="user-data m-b-30">
        <h3 class="title-3 m-b-30">
            <i class="zmdi zmdi-account-calendar"></i><a href="{{route('training_mentorship.create')}}"
                class="btn btn-primary btn-right">Add New Programme</a></h3>

        <div class="table-responsive table-data">
            <table class="table">
                <thead>
                    <tr>
                        <td>
                            <label class="au-checkbox">
                                <input type="checkbox">
                                <span class="au-checkmark"></span>
                            </label>
                        </td>
                        <td><b>image</b></td>
                        <td><b>Caption</b></td>
                        <td><b>Content</b></td>
                        <td></td>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($programmes as $programme)
                    <tr>
                        <td>
                            <label class="au-checkbox">
                                <input type="checkbox">
                                <span class="au-checkmark"></span>
                            </label>
                        </td>
                        <td>
                            <div class="table-data__info">
                                <h6><img src="{{ asset('images/training/' . $programme->photo) }}" alt="" style="height:70px">
                                </h6>
                            </div>
                        </td>

                        <td>
                            <div class="table-data__info">
                                <h6>{{$programme->name}}</h6>
                            </div>
                        </td>
                        <td>
                            <div class="table-data__info">
                                <h6>{{ substr($programme->content, 0, 50)}}{{ strlen($programme->content )> 50 ?"...":"" }}
                                </h6>
                            </div>
                        </td>
                        <td>
                            <div class="table-data-feature">
                                <a href="{{route('training_mentorship.edit', $programme->id)}}"><button class="item"
                                        type="submit" data-toggle="tooltip" data-placement="top" title="edit">
                                        <i class="zmdi zmdi-edit" style="color:blue"></i>
                                    </button></a>
                                <a href="{{route('training_mentorship.show', $programme->id)}}"><button class="item"
                                        type="submit" data-toggle="tooltip" data-placement="top" title="view">
                                        <i class="zmdi zmdi-eye" style="color:green"></i>
                                    </button></a>
                                {{Form::Open(['route'=>['training_mentorship.destroy',$programme->id], 'method'=>'delete'])}}
                                <button class="item" type="submit" data-toggle="tooltip" data-placement="top"
                                    title="Delete">
                                    <i class="zmdi zmdi-delete" style="color:red"></i>
                                </button>
                                {{Form::close()}}
                            </div>
                        </td>
                    </tr>
                    @empty
                    @endforelse


                </tbody>
            </table>
        </div>

    </div>
    <!-- END USER DATA-->
</div>

@endsection