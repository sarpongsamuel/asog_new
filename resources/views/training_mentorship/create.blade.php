@extends('Layouts.dashboard')
@push('styles')
{{ Html::style('dash/css/parsley.css') }}
@endpush
@section('title', '| Training & Mentorship-Create')
    
@section('content')
<div class="row">
        <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">Trainind And Mentorship</div>
                    <div class="card-body">
                        <div class="card-title">
                            <h3 class="text-center title-2">New programme</h3>
                        </div>
                        <hr>
                        {!! Form::open(['route' => 'training_mentorship.store', 'data-parsley-validate' => '', 'files'=>true]) !!}
                        
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        {{ Form::label('name', 'Programme Name', ['class' => 'control-label mb-1']) }}
                                        {{ Form::text('name', null, ['class' => 'form-control', 'required'=>''])}}
                                    </div>
                                </div>
                                
                                <div class="col-md-6">
                                        <div class="form-group">
                                            {{ Form::label('photo', 'Programme Photo', ['class' => 'control-label mb-1']) }} <br>   
                                            {{ Form::file('photo', null, ['class' => 'form-control', 'required'=>''])}}
                                        </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        {{ Form::label('content', 'Programme Content', ['class' => 'control-label mb-1']) }} <br>   
                                        {{ Form::textarea('content', null, ['class' => 'form-control', 'required'=>'', 'rows'=>'5'])}}
                                    </div>
                                </div>
                            </div>
                            
                            {!! Form::submit('Add programme', array('class' => 'btn btn-success btn-block', 'id'=>'btnSubmit')) !!} 
                        {{Form::close()}}
                    </div>
                </div>
            </div>

</div>
    
@endsection
@push('scripts')
{{ Html::script('dash/js/parsley.min.js') }}
@endpush