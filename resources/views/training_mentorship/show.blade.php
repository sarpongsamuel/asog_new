@extends('Layouts.dashboard')
@push('styles')
{{ Html::style('dash/css/parsley.css') }}
@endpush
@section('title', '| Training & Mentorship-Create')
    
@section('content')
<div class="row">
        <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">Trainind And Mentorship</div>
                    <div class="card-body">
                        <div class="card-title">
                            <h3 class="text-center title-2">Training programme</h3>
                        </div>
                        <hr>
                        {!! Form::model($training, ['route' => ['sponsor.update',$training->id],'data-parsley-validate' => '', 'files'=>true, 'method' => 'PUT']) !!}                          
                        
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        {{ Form::label('name', 'Programme Name', ['class' => 'control-label mb-1']) }}
                                        {{ Form::text('name', null, ['class' => 'form-control', 'required'=>'','disabled'=>true])}}
                                    </div>
                                </div>
                                
                                
                            </div>
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        {{ Form::label('content', 'Programme Content', ['class' => 'control-label mb-1']) }} <br>   
                                        {{ Form::textarea('content', null, ['class' => 'form-control', 'required'=>'', 'rows'=>'10','disabled'=>true])}}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                        <div class="form-group">
                                            {{ Form::label('photo', 'Programme Photo', ['class' => 'control-label mb-1']) }} <br> 
                                        <img src="{{asset('images/training/'.$training->photo)}}" alt="" class="img-responsives img-thumbnail" height="">  
                                            {{-- {{ Form::file('photo', null, ['class' => 'form-control', 'required'=>'','disabled'=>true])}} --}}
                                        </div>
                                </div>
                            </div>
                        <a href="{{route('training_mentorship.index')}}" class="btn btn-danger btn-block">Cancel</a>
                        {{Form::close()}}
                    </div>
                </div>
            </div>

</div>
    
@endsection
@push('scripts')
{{ Html::script('dash/js/parsley.min.js') }}
@endpush