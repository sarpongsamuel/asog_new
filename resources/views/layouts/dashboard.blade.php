<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>Dashboard @yield('title')</title>

    @include('partials._dash_styles')

</head>

<body class="animsition">
    <div class="page-wrapper">
        <!-- HEADER DESKTOP-->
        @include('partials._dash_header')
        <!-- END HEADER DESKTOP-->

        <!-- HEADER MOBILE-->
        @include('partials._dash_navigate')
        <!-- PAGE CONTENT-->
        <div class="page-content--bgf7">
            <!-- BREADCRUMB-->
            @include('partials._dash_breadcrumb')
            <!-- END BREADCRUMB-->

            <!-- WELCOME-->
            <section class="welcome p-t-10">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h1 class="title-4">Welcome back
                                <span>John!</span>
                            </h1>
                            <hr class="line-seprate">
                        </div>
                    </div>
                </div>
            </section>
            <!-- END WELCOME-->

            <!-- STATISTIC-->
            @yield('stats')
            <!-- END STATISTIC-->

            {{-- content here --}}
            <section>
                <div class="container">
                    @yield('content')
                </div>
            </section>

            <!-- COPYRIGHT-->
            @include('partials._dash_footer')
            <!-- END COPYRIGHT-->
        </div>

    </div>
    @include('partials._dash_scripts')
</body>

</html>
<!-- end document-->