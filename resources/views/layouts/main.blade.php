<!DOCTYPE html>
<html lang="en">
@include('partials._head')

<body>
    <!-- Preloader -->
    <div class="preloader"></div>

    <!-- Top Header_Area -->
    <section class="top_header_area">
        <div class="container">
            <ul class="nav navbar-nav top_nav">
                {{-- <li><a href="#"><i class="fa fa-phone"></i>+1 (168) 314 5016</a></li> --}}
                <li><a href="mailto:asoggh19@gmail.com"><i class="fa fa-envelope-o"></i>asoggh19@gmail.com</a></li>
                <li><a href="#"><i class="fa fa-calendar"></i>

                        @php
                        $date = date("Y");
                        $dd=$date;
                        $d = new DateTime($date);
                        echo $d->format('l');
                        @endphp
                        {{Carbon\Carbon::parse($date)->formatLocalized('%d,')}}
                        {{ substr(Carbon\Carbon::parse($date)->formatLocalized('%B'),0,3)}}
                    </a></li>

                <li><a href="#"><i class="fa fa-clock-o"></i>
                        @php
                        $time = Date('h:i:sa');
                        echo $time;
                        @endphp
                    </a></li>
            </ul>


            <ul class="nav navbar-nav navbar-right social_nav">
                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                @if (Auth::check())
                <li><a href="{{ route('logout') }}" onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                        <i class="fa fa-lock" aria-hidden="true"></i>
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </li>

                @else
                <li><a href="{{route('login')}}"><i class="fa fa-user" aria-hidden="true"></i></a></li>
                @endif
            </ul>
        </div>
    </section>
    <!-- End Top Header_Area -->

    <!-- Header_Area -->
    @include('partials._nav')
    <!-- Slider area -->
    {{-- @include('partials._slider') --}}
    <!-- End Slider area -->


    @yield('content')

    <!-- Footer Area -->
    @include('partials._footer')
    <!-- End Footer Area -->

    <!-- jQuery JS -->
    @include('partials._scripts')
</body>

</html>