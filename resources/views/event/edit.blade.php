@extends('Layouts.dashboard')
@section('title', '|Event-Create')

@push('styles')
{{ Html::style('dash/css/parsley.css') }}
@endpush

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">Edit Event</div>
            <div class="card-body">
                <div class="card-title">
                    <h3 class="text-center title-2">Edit Event</h3>
                </div>
                <hr>

                {!! Form::model($event, ['route' => ['event.update',$event->id],'data-parsley-validate' => '',
                'files'=>true, 'method' => 'PUT']) !!}
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            {{ Form::label('name', 'Event Name', ['class' => 'control-label mb-1']) }}
                            {{ Form::text('name', null, ['class' => 'form-control', 'required'=>'', 'autocomplete' => 'off'])}}
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            {{ Form::label('title', 'Host Title', ['class' => 'control-label mb-1']) }}
                            {{ Form::select('title_id', $titles, null, ['class' => 'form-control', 'required'] )}}

                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('host', 'Event Host', ['class' => 'control-label mb-1']) }}
                            {{ Form::text('host', null, ['class' => 'form-control', 'required'=>'', 'data-parsley-pattern' => "/^[a-zA-Z -\s]+$/", 'autocomplete' => 'off'])}}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('event_date', 'Event Date', ['class' => 'control-label mb-1']) }}
                            {{ Form::date('event_date', null, ['class' => 'form-control', 'required'=>'', 'autocomplete' => 'off'])}}
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('registration_ongoing', 'Registration State', ['class' => 'control-label mb-1']) }}
                            {{ Form::select('registration_ongoing', [0 => "Closed", 1 => 'Opened'], null, ['class' => 'form-control', 'required'] )}}

                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('venue', 'Venue', ['class' => 'control-label mb-1']) }}
                            {{ Form::text('event_venue', null, ['class' => 'form-control', 'required'=>'', 'autocomplete' => 'off'])}}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            {{ Form::label('content', 'Event Content', ['class' => 'control-label mb-1']) }}
                            {{ Form::textarea('content', null, ['class' => 'form-control', 'id' => 'wysiwyg-textarea', 'required'=>'', 'parsley-trigger'=>"keyup" ,'parsley-rangelength'=>"[20,500]",'autocomplete' => 'off', 'rows'=>'5'])}}
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('photo', 'Event Photo', ['class' => 'control-label mb-1']) }}
                            {{ Form::file('photo', null, ['class' => 'form-control', 'required'=>''])}}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <button class="btn btn-default form-control">Cancel Update</button>
                    </div>
                    <div class="col-md-6">
                        {!! Form::submit('Update Event', array('class' => 'btn btn-success btn-block',
                        'id'=>'btnSubmit'))
                        !!}
                    </div>
                </div>
                {{Form::close()}}
            </div>
        </div>
    </div>

</div>

@endsection
@push('scripts')
{{ Html::script('dash/js/parsley.min.js') }}
@endpush