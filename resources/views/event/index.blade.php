@extends('Layouts.dashboard')
@section('title', '| Event')

@section('content')
<div class="row">
    <div class="col-md-12">
        <!-- DATA TABLE -->
        <br>
        <h3 class="title-5 m-b-35">All Events</h3>
        <div class="table-data__tool">
            <div class="table-data__tool-left">
                <button class="au-btn-filter">
                    <i class="zmdi zmdi-filter-list"></i>filters</button>
            </div>
            <div class="table-data__tool-right">
                <a href="{{route('event.create')}}"><button class="au-btn au-btn-icon au-btn--green au-btn--small">
                        <i class="zmdi zmdi-plus"></i>add Event</button></a>
            </div>
        </div>
        <div class="table-responsive table-responsive-data2">
            <table class="table table-data2">
                <thead>
                    <tr>
                        <th>
                            <label class="au-checkbox">
                                <input type="checkbox">
                                <span class="au-checkmark"></span>
                            </label>
                        </th>
                        <th>Photo</th>
                        <th>Title/Name</th>
                        <th>Host</th>
                        <th>Venue</th>
                        <th>Date</th>
                        <th>Content</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($events as $event)

                    <tr class="tr-shadow">
                        <td>
                            <label class="au-checkbox">
                                <input type="checkbox">
                                <span class="au-checkmark"></span>
                            </label>
                        </td>
                        <td><img src="{{asset('images/event/'.$event->photo)}}" alt="" class="img-responsive"
                                style="height:80px; width:80px"></td>
                        <td>
                            <span class="block-email">{{$event->name}}</span>
                        </td>
                        <td class="desc">{{$event->title->title}} {{$event->host}}</td>
                        <td>{{$event->event_venue}}</td>
                        <td>
                            <span class="status--process">{{$event->event_date}}</span>
                        </td>
                        <td>
                            {!! substr($event->content, 0, 30)!!}{{ strlen($event->content )> 50 ?"...":"" }}
                        </td>
                        <td>
                            <div class="table-data-feature">
                                <button class="item" data-toggle="tooltip" data-placement="top" title="view">
                                    <a href="{{route('event.show',$event->id)}}"> <i class="zmdi zmdi-eye"
                                            style="color:blue"></i></a>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                    <a href="{{route('event.edit',$event->id)}}"><i class="zmdi zmdi-edit"
                                            style="color:green"></i></a>
                                </button>
                                {{Form::open(['route'=>['event.destroy',$event->id], 'method'=>'delete'])}}
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                    <i class="zmdi zmdi-delete" style="color:red"></i>
                                </button>
                                {{Form::close()}}

                            </div>
                        </td>
                    </tr>
                    <tr class="spacer"></tr>
                    @empty

                    @endforelse

                </tbody>
            </table>
        </div>
        <!-- END DATA TABLE -->
    </div>
</div>

@endsection