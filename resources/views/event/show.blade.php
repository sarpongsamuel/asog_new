@extends('Layouts.dashboard')
@section('title', '| Event-Details')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">Add New Event</div>
            <div class="card-body">
                <div class="card-title">
                    <h3 class="text-center title-2">New Event</h3>
                </div>
                <hr>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">Event</div>
                            <div class="card-body">
                                <div class="card-title">
                                    <h3 class="text-center title-2"> Event</h3>
                                </div>
                                <hr>

                                {{ Form::model($event, array('route' => array('event.update', $event->id))) }}
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            {{ Form::label('name', 'Event Name', ['class' => 'control-label mb-1']) }}
                                            {{ Form::text('name', null, ['class' => 'form-control', 'disabled'=>'true'])}}
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            {{ Form::label('title', 'Host Title', ['class' => 'control-label mb-1']) }}
                                            {{ Form::select('title_id', $titles, null, ['class' => 'form-control', 'disabled'=>'true'] )}}
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            {{ Form::label('host', 'Event Host', ['class' => 'control-label mb-1']) }}
                                            {{ Form::text('host', null, ['class' => 'form-control', 'disabled'=>'true'])}}
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            {{ Form::label('event_date', 'Event Date', ['class' => 'control-label mb-1']) }}
                                            {{ Form::date('event_date', null, ['class' => 'form-control', 'disabled'=>'true'])}}
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            {{ Form::label('registration_ongoing', 'Registration State', ['class' => 'control-label mb-1']) }}
                                            {{ Form::select('registration_ongoing', [0 => "Closed", 1 => 'Opened'], null, ['class' => 'form-control', 'disabled'=>'true'] )}}
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            {{ Form::label('venue', 'Venue', ['class' => 'control-label mb-1']) }}
                                            {{ Form::text('event_venue', null, ['class' => 'form-control', 'disabled'=>'true'])}}
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            {{ Form::label('content', 'Event Content', ['class' => 'control-label mb-1']) }}
                                            {{ Form::textarea('content', null, ['class' => 'form-control', 'disabled'=>'true'])}}
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            {{ Form::label('photo', 'Event Photo', ['class' => 'control-label mb-1']) }}
                                            <br>
                                            <img src="{{asset('images/event/'.$event->photo)}}" alt="img"
                                                class="img-responsive img-thumbnail" style="height:250px">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <a href="{{route('event.index')}}" class="btn btn-success btn-block">Ok</a>
                                    </div>
                                </div>
                                {{Form::close()}}
                            </div>
                        </div>
                    </div>

                </div>



                @endsection