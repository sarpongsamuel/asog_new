@extends('Layouts.dashboard')
@section('title', '| Title')

@section('content')
<div class="col-lg-12">
    <!-- USER DATA-->
    @include('partials._message')

    <div class="user-data m-b-30">
        <h3 class="title-3 m-b-30">
            <i class="zmdi zmdi-account-calendar"></i>Title</h3>


        {!! Form::open(['route' => 'title.store','data-parsley-validate' => '']) !!}
        <div class="form-group">
            <div class="row">
                <div class="col md-6 offset-2">
                    {{ Form::text('title', null, ['class' => 'form-control ', 'required'=>'', 'data-parsley-pattern' => "/^[a-zA-Z -\s]+$/", 'placeholder' => 'enter title here...', 'autocomplete' => 'off'])}}
                </div>
                <div class="col-md-3">
                    {{Form::submit('Add title',['class'=>'btn btn-primary'])}}
                </div>
            </div>
        </div>
        {!! Form::close() !!}

        <div class="table-responsive table-data">
            <table class="table">
                <thead>
                    <tr>
                        <td>
                            <label class="au-checkbox">
                                <input type="checkbox">
                                <span class="au-checkmark"></span>
                            </label>
                        </td>
                        <td><b>Title</b></td>

                        <td></td>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($titles as $title)
                    <tr>
                        <td>
                            <label class="au-checkbox">
                                <input type="checkbox">
                                <span class="au-checkmark"></span>
                            </label>
                        </td>
                        <td>
                            <div class="table-data__info">
                                <h6>{{$title->title}}</h6>
                            </div>
                        </td>
                        <td>
                            <div class="table-data-feature">
                                {{Form::Open(['route'=>['title.destroy',$title->id], 'method'=>'delete'])}}
                                <button class="item" type="submit" data-toggle="tooltip" data-placement="top"
                                    title="Delete">
                                    <i class="zmdi zmdi-delete"></i>
                                </button>
                                {{Form::close()}}
                            </div>
                        </td>
                    </tr>
                    @empty
                    @endforelse


                </tbody>
            </table>
        </div>

    </div>
    <!-- END USER DATA-->
</div>

@endsection