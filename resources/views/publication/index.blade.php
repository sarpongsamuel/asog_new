@extends('Layouts.dashboard')
@section('title', '| publications')

@section('content')
<div class="col-lg-12">
    <!-- USER DATA-->
    @include('partials._message')

    <div class="user-data m-b-30">
        <h3 class="title-3 m-b-30">
            <i class="zmdi zmdi-account-calendar"></i><a href="{{route('publication.create')}}"
                class="btn btn-primary btn-right">Add New Publication</a></h3>

        <div class="table-responsive table-data">
            <table class="table">
                <thead>
                    <tr>
                        <td>
                            <label class="au-checkbox">
                                <input type="checkbox">
                                <span class="au-checkmark"></span>
                            </label>
                        </td>
                        <td><b></b></td>
                        <td><b>Title</b></td>
                        <td><b>Category</b></td>
                        <td><b>Author</b></td>
                        <td><b>Publication Date</b></td>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($publications as $publication)
                    <tr>
                        <td>
                            <label class="au-checkbox">
                                <input type="checkbox">
                                <span class="au-checkmark"></span>
                            </label>
                        </td>
                        <td>
                            <div class="table-data__info">
                                <h6><img src="{{ asset('publication/pdf.png') }}" alt="" style="height:30px"></h6>
                            </div>
                        </td>
                        <td>
                            <div class="table-data__info">
                                <h6>{{ substr($publication->title, 0, 30)}}{{ strlen($publication->title )> 30 ?"...":"" }}
                                </h6>
                            </div>
                        </td>
                        <td>
                            <div class="table-data__info">
                                <h6>{{$publication->publicationCategory->name}}</h6>
                            </div>
                        </td>
                        <td>
                            <div class="table-data__info">
                                <h6>{{$publication->author}}</h6>
                            </div>
                        </td>
                        >
                        <td>
                            <div class="table-data__info">
                                <h6>{{$publication->publication_date}}</h6>
                            </div>
                        </td>
                        <td>
                            <div class="table-data-feature">
                                <a href="{{route('publication.edit',$publication->id)}}"><button class="item"
                                        type="submit" data-toggle="tooltip" data-placement="top" title="Delete">
                                        <i class="zmdi zmdi-eye" style="color:green"></i>
                                    </button></a>
                                {{Form::Open(['route'=>['publication.destroy',$publication->id], 'method'=>'delete'])}}
                                <button class="item" type="submit" data-toggle="tooltip" data-placement="top"
                                    title="Delete">
                                    <i class="zmdi zmdi-delete" style="color:red"></i>
                                </button>
                                {{Form::close()}}

                            </div>
                        </td>
                    </tr>
                    @empty
                    @endforelse


                </tbody>
            </table>
        </div>

    </div>
    <!-- END USER DATA-->
</div>

@endsection