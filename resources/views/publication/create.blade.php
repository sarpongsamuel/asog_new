@extends('Layouts.dashboard')
@section('title', '| Publication')
@push('styles')
{{ Html::style('dash/css/parsley.css') }}
@endpush
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">Add New Publication</div>
            <div class="card-body">
                <div class="card-title">
                    <h3 class="text-center title-2">New Publication</h3>
                </div>
                <hr>
                {!! Form::open(['route' => 'publication.store', 'data-parsley-validate' => '', 'files'=>true]) !!}

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('title', 'Pub. Title', ['class' => 'control-label mb-1']) }}
                            {{ Form::text('pub_title', null, ['class' => 'form-control', 'required'=>''])}}
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('author', 'Author', ['class' => 'control-label mb-1']) }}
                            {{ Form::text('author', null, ['class' => 'form-control', 'required'=>''])}}
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('pub_date', 'Publication Date', ['class' => 'control-label mb-1']) }}
                            {{ Form::text('publication_date', null, ['class' => 'form-control', 'required'=>''])}}
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('publication_category_id', 'Publication Category')}}
                            {{ Form::select('publication_category_id', $categories, null, ['class' => 'form-control', 'required','placeholder'=>'-- select publication Category --'] )}}
                        </div>
                    </div>
                    {{-- <div class="col-md-5">
                                        <div class="form-group">
                                            {{ Form::label('associate_school', 'Associate School')}}
                    {{ Form::select('associate_school_id', $schools, null, ['class' => 'form-control', 'required','placeholder'=>'-- select associate school --'] )}}
                </div>
            </div> --}}
            <div class="col-md-3">
                <div class="form-group">
                    {{ Form::label('document', 'Document', ['class' => 'control-label mb-1']) }}
                    {{ Form::file('document', null, ['class' => 'form-control', 'required'=>''])}}
                </div>
            </div>

        </div>

        {!! Form::submit('Add To Publication', array('class' => 'btn btn-success btn-block', 'id'=>'btnSubmit')) !!}
        {{Form::close()}}
    </div>
</div>
</div>

</div>

@endsection
@push('scripts')
{{ Html::script('dash/js/parsley.min.js') }}
@endpush