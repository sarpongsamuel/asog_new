@extends('Layouts.dashboard')
@section('title', '| Constitution')

@section('content')
<div class="col-lg-12">
    <!-- USER DATA-->
    @include('partials._message')

    <div class="user-data m-b-30">
        <h3 class="title-3 m-b-30">
            <i class="zmdi zmdi-account-calendar"></i>Constitution</h3>


        {!! Form::open(['route' => 'constitution.store','data-parsley-validate' => '', 'files'=>true]) !!}
        <div class="form-group">
            <div class="row">
                <div class="col md-6 offset-2">
                    {{ Form::file('constitution', null, ['class' => 'form-control ', 'required'=>''])}}
                </div>
                <div class="col-md-4">
                    {{Form::submit('upload Constitution',['class'=>'btn btn-primary'])}}
                </div>
            </div>
        </div>
        {!! Form::close() !!}

        <div class="table-responsive table-data">
            <table class="table">
                <thead>
                    <tr>
                        <td>
                            <label class="au-checkbox">
                                <input type="checkbox">
                                <span class="au-checkmark"></span>
                            </label>
                        </td>
                        <td><b>File</b></td>
                        {{-- <td><b>Title</b></td> --}}
                        <td></td>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($constitution as $doc)
                    <tr>
                        <td>
                            <label class="au-checkbox">
                                <input type="checkbox">
                                <span class="au-checkmark"></span>
                            </label>
                        </td>
                        <td>
                            <div class="table-data__info">
                                <h6><img src="{{ asset('publication/pdf.png') }}" alt="" style="height:30px"></h6>
                            </div>
                        </td>
                        <td>
                            <div class="table-data__info">
                                <h6>{{$doc->name}}</h6>
                            </div>
                        </td>
                        <td>
                            <div class="table-data-feature">
                                {{Form::Open(['route'=>['constitution.destroy',$doc->id], 'method'=>'delete'])}}
                                <button class="item" type="submit" data-toggle="tooltip" data-placement="top"
                                    title="Delete">
                                    <i class="zmdi zmdi-delete"></i>
                                </button>
                                {{Form::close()}}
                            </div>
                        </td>
                    </tr>
                    @empty
                    @endforelse


                </tbody>
            </table>
        </div>

    </div>
    <!-- END USER DATA-->
</div>

@endsection