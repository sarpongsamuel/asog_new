@extends('Layouts.dashboard')
@section('title', '| Our Mission')
    
@section('content')
<div class="row">
        <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">Our mission</div>
                    <div class="card-body">
                        <div class="card-title">
                            <h3 class="text-center title-2">Our mission</h3>
                        </div>
                        <hr>
                        {!! Form::model($mission, ['route' => ['mission.update',$mission->id],'data-parsley-validate' => '','method' => 'PUT']) !!}                          
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        {{ Form::label('context', 'Mission', ['class' => 'control-label mb-1']) }} <br>   
                                        {{ Form::textarea('context', null, ['class' => 'form-control', 'required'=>'', 'rows'=>'10'])}}
                                    </div>
                                </div>
                            </div>
                            
                            
                            {!! Form::submit('Update mission', array('class' => 'btn btn-success btn-block', 'id'=>'btnSubmit')) !!} 
                        {{Form::close()}}
                    </div>
                </div>
            </div>

</div>
    
@endsection