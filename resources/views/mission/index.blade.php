@extends('Layouts.dashboard')
@section('title', '| Our Mission')

@section('content')
<div class="col-lg-12">
    <!-- USER DATA-->
    @include('partials._message')

    <div class="user-data m-b-30">
        <h3 class="title-3 m-b-30">
            <a href="{{route('mission.create')}}" class="btn btn-success"><span class="fa fa-plus"></span>Add
                Mission</a>

        </h3>

        <div class="table-responsive table-data">
            <div class="text-center">
                @forelse ($mission as $mission)
                <h3 class="title-3 m-b-30">
                    {{-- <a href="{{route('mission.create')}}" class="btn btn-success"><span
                        class="fa fa-plus"></span>Add Mission</a> --}}
                    {{Form::open(['route'=>['mission.destroy',$mission->id], 'method'=>'delete', 'class'=>'inline-form'])}}
                    <button class="item btn btn-warning" data-toggle="tooltip" data-placement="top" title="Delete"><span
                            class="fa fa-trash"></span>delete </button>
                    <a href="{{route('mission.edit',$mission->id)}}" class="btn btn-danger"><span
                            class="fa fa-edit"></span> Edit Mission</a>
                    {{Form::close()}}
                </h3>

                <p class="mr-10" style="margin-top:5%; padding:20px">{{$mission->context}}</p>
                @empty

                @endforelse
            </div>
        </div>

    </div>
    <!-- END USER DATA-->
</div>

@endsection