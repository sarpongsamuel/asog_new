@extends('Layouts.dashboard')
@section('title', '| Our Mission')
    
@section('content')
<div class="row">
        <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">Our Mission</div>
                    <div class="card-body">
                        <div class="card-title">
                        </div>
                        <hr>
                        {!! Form::open(['route' => 'mission.store', 'data-parsley-validate' => '', 'files'=>true]) !!}
                        
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        {{ Form::label('context', 'Mission', ['class' => 'control-label mb-1']) }} <br>   
                                        {{ Form::textarea('context', null, ['class' => 'form-control', 'required'=>'', 'rows'=>'5'])}}
                                    </div>
                                </div>
                            </div>
                            
                            
                            {!! Form::submit('Add New mission', array('class' => 'btn btn-success btn-block', 'id'=>'btnSubmit')) !!} 
                        {{Form::close()}}
                    </div>
                </div>
            </div>

</div>
    
@endsection