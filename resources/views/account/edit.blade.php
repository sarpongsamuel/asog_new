@extends('Layouts.dashboard')
@section('title', '| New Account')
@push('styles')
{{ Html::style('dash/css/parsley.css') }}
@endpush
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">Add New Account</div>
            <div class="card-body">
                <div class="card-title">
                    <h3 class="text-center title-2">New Account</h3>
                </div>
                <hr>
                {!! Form::model($account, ['route' => ['account.update',$account->id],'data-parsley-validate' =>
                '','method'
                => 'PUT']) !!}
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('name', 'Username', ['class' => 'control-label mb-1']) }}
                            {{ Form::text('name', null, ['class' => 'form-control', 'required'=>'','data-parsley-pattern' => "/^[a-zA-Z -\s]+$/"])}}
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('email', 'Email', ['class' => 'control-label mb-1']) }}
                            {{ Form::text('email', null, ['class' => 'form-control', 'required'=>''])}}
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('password', 'Password', ['class' => 'control-label mb-1']) }}
                            {{ Form::text('password', null, ['class' => 'form-control', 'required'=>'', 'data-parsley-pattern' => "/^[a-zA-Z -\s]+$/"])}}
                        </div>
                    </div>
                </div>
                {!! Form::submit('Update Account', array('class' => 'btn btn-success btn-block', 'id'=>'btnSubmit'))
                !!}
                {{Form::close()}}
            </div>
        </div>
    </div>

</div>

@endsection
@push('scripts')
{{ Html::script('dash/js/parsley.min.js') }}
@endpush