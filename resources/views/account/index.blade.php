@extends('Layouts.dashboard')
@section('title', '| Accounts')

@section('content')
<div class="col-lg-12">
    <!-- USER DATA-->
    @include('partials._message')

    <div class="user-data m-b-30">
        <h3 class="title-3 m-b-30">
            <i class="zmdi zmdi-account"></i>Account
        </h3>
        <h3 class="title-3 m-b-30">
            <a href="{{route('account.create')}}" class="btn btn-primary">Add New Account</a>
        </h3>

        <div class="table-responsive table-data">
            <table class="table">
                <thead>
                    <tr>
                        <td>
                            <label class="au-checkbox">
                                <input type="checkbox">
                                <span class="au-checkmark"></span>
                            </label>
                        </td>
                        <td><b>Name</b></td>
                        <td><b>Email</b></td>
                        <td><b>Role</b></td>
                        <td><b></b></td>
                        <td></td>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($accounts as $account)
                    <tr>
                        <td>
                            <label class="au-checkbox">
                                <input type="checkbox">
                                <span class="au-checkmark"></span>
                            </label>
                        </td>
                        <td>
                            <div class="table-data__info">
                                <h6>{{$account->name}}</h6>
                            </div>
                        </td>
                        <td>
                            <div class="table-data__info">
                                <h6>{{$account->email}}</h6>
                            </div>
                        </td>
                        <td>
                            <div class="table-data__info">

                                @if ($account->is_admin)
                                <h6>Admin</h6>
                                @else
                                <h6>Guest</h6>
                                @endif
                            </div>
                        </td>
                        <td>
                            <div class="table-data-feature">
                                @if ($account->is_admin)
                                <span style="color:grey">denied</span>
                                @else

                                <a href="{{route('account.edit', $account->id)}}"><button class="item" type="button"
                                        data-toggle="tooltip" data-placement="top" title="Delete">
                                        <i class="zmdi zmdi-edit" style="color:green"></i>
                                    </button></a>
                                {{Form::Open(['route'=>['account.destroy',$account->id], 'method'=>'delete'])}}
                                <button class="item" type="submit" data-toggle="tooltip" data-placement="top"
                                    title="Delete">
                                    <i class="zmdi zmdi-delete" style="color:red"></i>
                                </button>
                                {{Form::close()}}

                                @endif

                            </div>
                        </td>
                    </tr>
                    @empty
                    @endforelse


                </tbody>
            </table>
        </div>

    </div>
    <!-- END USER DATA-->
</div>

@endsection