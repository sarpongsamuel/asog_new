@extends('layouts.main')
@section('content')
<!-- Banner area -->
<section class="banner_area" data-stellar-background-ratio="0.5">
	<h2>Publications</h2>
	<ol class="breadcrumb">
		<li><a href="{{route('pages.index')}}">Home</a></li>
		<li><a href="{{route('pages.publications')}}" class="active">Publications</a></li>
	</ol>
</section>
<section class="our_services_tow">
	<div class="container">
		<div class="architecture_area services_pages">
			<div class="portfolio_filter portfolio_filter_2">
				<ul>
					<li data-filter="*"><a href=""><i class="fa fa-folder-open" aria-hidden="true"></i>All</a></li>
					<li data-filter=".painting"><a href=""><i class="fa fa-folder-open" aria-hidden="true"></i>Book Of
							Abstract</a>
					</li>
					<li data-filter=".webdesign"><a href=""><i class="fa fa-folder-open" aria-hidden="true"></i>Research
							Proposal</a>
					</li>


					@forelse ($categories as $category)
					@if ($category->publications->count() > 0)
					<li data-filter=".category{{$category->id}}"><a href=""><i class="fa fa-certificate"
								aria-hidden="true"></i>{{$category->name}}</a></li>
					@endif

					@empty
					{{-- <h2>No publications available</h2> --}}
					@endforelse
				</ul>
			</div><br><br>

			{{-- search  publication here --}}
			<div class="searchForm ">
				<form action="{{route('pages.search')}}">
					<h4 class="text">Search Publication</h4><br>
					<div class="input-group">
						<input type="search" name="query" class="form-control" placeholder="Type & Hit Enter"
							style="text-align:left">
						<span class="input-group-addon"><i class="fa fa-search"></i></span>
					</div>
				</form>
			</div>
			{{-- end of search --}}

			{{-- start of book of abstracts --}}
			@if($abstracts->Count() > 0)

			<div class="portfolio_item portfolio_2">
				<div class="grid-sizer-2"></div>
				<div class="single_facilities col-sm-7 painting ">
					<div class="who_we_area">
						<div class="subtittle">
							<h2>Book Of Abstract</h2>
						</div>
						<div class="table">
							<div class="table-responsive-sm">
								<table class="table table-bordered wow animated fadeIn" data-wow-duration="2s"
									data-wow-delay="0.7s">
									<thead>
										<tr>
											<th>File</th>
											<th>Conference Year</th>
											<th>Download</th>
										</tr>
									</thead>
									<tbody style="text-align:center">
										@foreach ($abstracts as $abstract)
										<tr>
											<td>
												<span class="fa fa-file" style="color:red"></span>
											</td>
											<td>{{$abstract->conference_year}}</td>
											<td>
												<a href="{{Storage::url('public/abstractResearch/'.$abstract->document)}}">
													<span class="fa fa-cloud-download"></span>
												</a>
											</td>
										</tr>
										@endforeach

									</tbody>
								</table>
							</div>
						</div>


					</div>
				</div><br>
				@endif
				{{-- end of book of abstract --}}


				{{-- research proposals here --}}
				@if($proposals->Count() > 0)

				<div class="single_facilities col-sm-7 webdesign ">
					<div class="who_we_area">
						<div class="subtittle">
							<h2>Research Proposals</h2>
						</div>
						<div class="table">
							<div class="table-responsive-sm">
								<table class="table table-bordered wow animated fadeIn" data-wow-duration="2s"
									data-wow-delay="0.7s" style="border-bottom:20%">
									<thead>
										<tr>
											<th>File</th>
											<th>Conference Year</th>
											<th>Download</th>
										</tr>
									</thead>
									<tbody style="text-align:center">
										@foreach ($proposals as $proposal)
										<tr>
											<td>
												<a href="hey"><span class="fa fa-file" style="color:red"></span></a>
											</td>
											<td>{{$proposal->conference_year}}</td>
											<td>
												<a href="{{asset('storage/abstractResearch/'.$proposal->document)}}">
													<span class="fa fa-cloud-download"></span>
												</a>
											</td>
										</tr>
										@endforeach

									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div><br><br><br>
				@endif
				{{-- end of research proposals --}}

				{{-- loop through varying content here --}}
				@foreach ($categories as $category)
				@if ($category->publications->count() > 0)

				<div class="single_facilities col-sm-7 category{{$category->id}} ">
					<div class="who_we_area">
						<div class="subtittle">
							<h2>{{$category->name}}</h2>
						</div>
						@php
						$sortedPublications = collect($category->publications)->sortByDesc('created_at')
						@endphp
						@forelse ($sortedPublications as $publication)
						<div class="row tab wow slideInLeft">
							<div class="col-md-3">
								<img src="{{ asset('publication/pdf.png') }}" alt="" class="img-responsive"
									style="height:70px; margin-top:40px; margin-left:40px;"><br>
								<a href="{{asset('storage/publication/'.$publication->document)}}">
									<span class="text-center" style="margin-left:35px">
										<i class="fa fa-cloud-download"></i>
										download
									</span>
								</a>

							</div>
							<div class="col-md-9">
								<div class="row">
									<div class="col-md-12 r1"><b>Author(s):</b> {{$publication->author}}</div>
									<hr style="border:1px solid #dcdcdc;">
									<div class="col-md-12 r1"><b>Year Of Publication:</b>
										{{$publication->publication_date}}
									</div>
									<hr style="border:1px solid #dcdcdc;">
									<div class="col-md-12 r1"><b>Title: </b>{{$publication->title}}</div>
									<hr>

								</div>
							</div>
						</div>
						<hr style="">
						@empty

						@endforelse
						<br>
					</div>
				</div>
				@endif

				@endforeach

			</div>
		</div>
	</div>
</section>


@endsection