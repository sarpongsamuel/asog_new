@extends('layouts.main')
@section('content')
<!-- Banner area -->
<section class="banner_area" data-stellar-background-ratio="0.5">
    <h2>Sponsors</h2>
    <ol class="breadcrumb">
        <li><a href="{{route('pages.index')}}">Home</a></li>
        <li><a href="{{route('pages.sponsors')}}" class="active">Sponsors</a></li>
    </ol>
</section>

<section class="all_contact_info">
    <div class="container">

        {{-- display sponsors here --}}
        @forelse ($sponsors as $sponsor)
        <div class="row m-t-50">
            <div class="col-md-3 ">
                <img src="{{asset('images/sponsor/'.$sponsor->logo)}}" class="img-responsive" alt="">
            </div>
            <div class="col-md-7 col-md-offset-1">
                <h3>{{$sponsor->name}}</h3>
                {!!$sponsor->details!!}
                <p>
                    <span class="fa fa-map-marker"> {{$sponsor->location}}</span><br>
                    <span class="fa fa-phone"> {{$sponsor->contact1}}/ {{$sponsor->contact2}}</span>
                </p>
            </div>
        </div>
        <hr>
        @empty

        @endforelse
    </div>
</section>
<!-- All contact Info -->
<section class="all_contact_info">
    <div class="container">
        <div class="row contact_row">
            <div class="col-sm-12 contact_info send_message">
                <h2>Become A Sponsor</h2>
                @include('partials._message')
                {!! Form::open(['route' => 'newsponsor.store', 'data-parsley-validate' => '', 'files'=>true,
                'class'=>'form-inline contact_box']) !!}
                {{ Form::text('fullname', null, ['class' => 'form-control input_box', 'required'=>'', 'autocomplete' => 'on', 'placeholder'=>'Full Name*'])}}
                {{ Form::text('company', null, ['class' => 'form-control input_box', 'required'=>'', 'autocomplete' => 'on', 'placeholder'=>'Company Name *'])}}
                {{ Form::email('email', null, ['class' => 'form-control input_box', 'required'=>'', 'autocomplete' => 'on', 'placeholder'=>'Email *'])}}
                {{ Form::number('telephone', null, ['class' => 'form-control input_box', 'required'=>'', 'autocomplete' => 'on', 'placeholder'=>'Telephone*'])}}
                {{ Form::textarea('message', null, ['class' => 'form-control input_box', 'required'=>'','placeholder'=>'Message*','rows'=>'5'])}}
                <button type="submit" class="btn btn-default">Send Message</button>
                {{Form::close()}}
            </div>
        </div>
    </div>
</section>
<!-- End All contact Info -->


@endsection