@extends('layouts.main')
@section('content')
<!-- Banner area -->
<section class="banner_area" data-stellar-background-ratio="0.5">
	<h2>KNUST</h2>
	<ol class="breadcrumb">
		<li><a href="{{route('pages.index')}}">Home</a></li>
		<li><a href="{{route('pages.events')}}" class="active">{{$school->name}}</a></li>
	</ol>
</section>
{{-- leaders --}}


{{-- publications --}}
<section class="our_team_area">
	<div class="container">
		@forelse ($categories as $category)
		<div class="wow animated fadeInUp" data-wow-delay="0.2s">
			<div class="tittle">
				<h2>{{$category->name}}</h2>
			</div>
			<div class="row team_row">
				<table class="table">
					<thead class="thead-dark">
						<tr>
							<th>File</th>
							<th>Title</th>
							<th>Author</th>
							<th>Co-Author(s)</th>
							<th>Uploaded at</th>
							<th>Download</th>
						</tr>
					</thead>
					<tbody>
						@forelse ($publications as $publication)
						@if ($publication->publication_category_id == $category->id)
						<tr>
							<td><span class="fa fa-file"></span></td>
							<td>{{$publication->title}}</td>
							<td>{{$publication->author}}</td>
							<td>{{$publication->co_author}}</td>
							<td>{{Carbon\Carbon::parse($publication->created_at)->format('m/d/Y')}}</td>
							<td>
								<a href="{{asset('storage/publication/'.$publication->document)}}"
									class="btn btn-success"><span class="fa fa-download"> download</span></a>
							</td>
						</tr>
						@endif

						@empty

						@endforelse
					</tbody>
				</table>
				<br>

			</div>
		</div>
		@empty

		@endforelse
	</div>
</section>
<!-- End Our Team Area -->

@endsection