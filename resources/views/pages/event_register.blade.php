@extends('layouts.main')

@section('content')
<!-- Banner area -->
<section class="banner_area" data-stellar-background-ratio="0.5">
    <h2>Registration</h2>
    <ol class="breadcrumb">
        <li><a href="{{route('pages.index')}}">Home</a></li>
        <li><a href="{{route('pages.sponsors')}}" class="active">Registration</a></li>
    </ol>
</section>

<!-- All contact Info -->
<section class="all_contact_info">
    <div class="container">
        <div class="row contact_row">
            <div class="col-sm-12 contact_info send_message">
                <h2>Registration Form</h2>
                @include('partials._message')

                {!! Form::open(['route' => 'pages.event_register.store', 'data-parsley-validate' => '', 'class' =>
                'form-inline
                contact_box']) !!}
                {{ Form::select('title', $titles, null, ['placeholder' => 'Title', 'class' => 'form-control input_box', 'required'=>'']) }}

                {{ Form::hidden('event_id', $event->id)}}
                {{ Form::text('last_name', null, ['class' => 'form-control input_box', 'placeholder' => 'Last Name *', 'required'=>'', 'data-parsley-pattern' => "/^[a-zA-Z -\s]+$/", 'autocomplete' => 'off'])}}
                {{ Form::text('other_names', null, ['class' => 'form-control input_box', 'placeholder' => 'Other Names *', 'required'=>'', 'data-parsley-pattern' => "/^[a-zA-Z -\s]+$/", 'autocomplete' => 'off'])}}
                {{ Form::text('institution', null, ['class' => 'form-control input_box', 'placeholder' => 'Institution *', 'required'=>'', 'autocomplete' => 'off'])}}
                {{ Form::email('email', null, ['class' => 'form-control input_box', 'placeholder' => 'Email *', 'required'=>'', 'autocomplete' => 'off'])}}
                {{ Form::text('telephone_1', null, ['class' => 'form-control input_box', 'placeholder' => 'Telephone 1 *', 'required'=>'', 'data-parsley-type' => "digits", 'autocomplete' => 'off'])}}
                {{ Form::text('telephone_2', null, ['class' => 'form-control input_box', 'placeholder' => 'Telephone 2', 'data-parsley-type' => "digits", 'autocomplete' => 'off'])}}
                {{ Form::text('address', null, ['class' => 'form-control input_box', 'placeholder' => 'Postal Address*', 'required'=>'', 'autocomplete' => 'off'])}}
                {{ Form::text('name_order', null, ['class' => 'form-control input_box', 'placeholder' => 'Please indicate the how you would like your name to appear on your badge/tag (e.g. Mr John Doe) *', 'required'=>'', 'autocomplete' => 'off'])}}
                {{ Form::select('status', ['Undergraduate and Postgraduate Student' => 'Undergraduate and Postgraduate Student (Registration per Person GHS 150)', 'Senior Staff' => 'Senior Staff (Registration per Person GHS 200)', 'Senior Member' => 'Senior Member (Registration per Person GHS 250)', 'Non Member' => 'Non Member (Registration per Person GHS 350)'], null, ['placeholder' => 'I am a/an', 'class' => 'form-control input_box', 'required'=>'']) }}
                {{ Form::select('accommodation', ['GUSS COMPLEX Standard Room (Self-catering) Price per Person per Day = GHS 35.00' => 'GUSS COMPLEX Standard Room (Self-catering) Price per Person per Day = GHS 35.00', 'SMD Guest House Single B/B Price per Person per Day = GHS 80.00'=> 'SMD Guest House Single B/B Price per Person per Day = GHS 80.00', 'SMD Guest House Double B/B Price per Person per Day = GHS 100.00' => 'SMD Guest House Double B/B Price per Person per Day = GHS 100.00', 'KCCR Guest House Standard Room (self-catering) Price per Person per Day = GHS 150.00' => 'KCCR Guest House Standard Room (self-catering) Price per Person per Day = GHS 150.00', 'IPO Guest House Standard Room (self-catering) Price per Person per Day = GHS 150.00' => 'IPO Guest House Standard Room (self-catering) Price per Person per Day = GHS 150.00', 'Engineering Guest House Standard Room B/B Price per Person per Day = GHS 180.00' => 'Engineering Guest House Standard Room B/B Price per Person per Day = GHS 180.00', 'Other (I will provide my own accommodation)' => 'Other (I will provide my own accommodation)'], null, ['placeholder' => 'Accommodation', 'class' => 'form-control input_box', 'required'=>'']) }}
                {{ Form::select('num_of_days_staying', [1 => '1 day', 2=> '2 days', 3 => '3 days'], null, ['placeholder' => 'I am staying for *', 'class' => 'form-control input_box', 'required'=>'']) }}
                {{ Form::text('registration_code', null, ['class' => 'form-control input_box', 'placeholder' => 'Regsitration Code *', 'required'=>'', 'autocomplete' => 'off'])}}
                <button type="submit" class="btn btn-default">Register</button>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</section>
@endsection