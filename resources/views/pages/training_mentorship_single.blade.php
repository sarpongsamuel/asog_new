@extends('layouts.main')
@section('content')
     <!-- Banner area -->
     <section class="banner_area" data-stellar-background-ratio="0.5">
            <h2>Training & Mentorship</h2>
            <ol class="breadcrumb">
                <li><a href="{{route('pages.index')}}">Home</a></li>
                <li><a href="{{route('pages.training_mentorship')}}" class="active">Training & Mentorship</a></li>
            </ol>
    </section>
    
    <section class="building_construction_area">
            <div class="container">
                <div class="row building_construction_row">
                    {{-- training content --}}
                    {{-- side toggle --}}
                    <div class="col-sm-4 constructing_right">
                            <h2>Quick Links</h2>
                            <ul class="painting">
                                <li><a href="{{route('pages.events')}}"><i class="fa fa-diamond" aria-hidden="true"></i>EVENTS</a></li>
                                <li><a href="{{route('pages.about')}}"><i class="fa fa-tags" aria-hidden="true"></i>ABOUT US</a></li>
                                <li><a href="{{route('pages.galleries')}}"><i class="fa fa-image" aria-hidden="true"></i>GALLERY</a></li>
                                <li><a href="{{route('pages.sponsors')}}"><i class="fa fa-thumbs-up" aria-hidden="true"></i>BECOME A SPONSOR</a></li>
                            </ul>
                            {{-- <div class="contact_us">
                                <h4>Contact Us</h4>
                                <a href="#" class="contac_namber">+88 0168 3661882</a>
                                <a href="#" class="contac_namber">+88 0181 2013370</a>
                                <p>Lorem Ipsum is simply dummy text of the print-ing and typesetting industry. If you use this site regularly and would like to help keep</p>
                                <a href="#" class="button_all">Contact Us</a>
                            </div> --}}
                        </div>
                    
                    <div class="col-sm-8 constructing_right">
                    <h2><a href="#" style="color:#067000; ">{{$programme->name}}</a></h2><br>
                            <img src="{{asset('images/training/'.$programme->photo)}}" alt="">
                            <br><br>
                            <p>{{$programme->content}}</p><br><br>
                            
                            
                        </div>
                    
                </div>
            </div>
        </section>

@endsection