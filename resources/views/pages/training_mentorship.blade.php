@extends('layouts.main')
@section('content')
     <!-- Banner area -->
     <section class="banner_area" data-stellar-background-ratio="0.5">
            <h2>Training & Mentorship</h2>
            <ol class="breadcrumb">
                <li><a href="{{route('pages.index')}}">Home</a></li>
                <li><a href="{{route('pages.training_mentorship')}}" class="active">Training & Mentorship</a></li>
            </ol>
    </section>
    
    <section class="building_construction_area">
            <div class="container">
                <div class="row building_construction_row">
                    {{-- training content --}}
                    
                    @forelse ($programmes as $programme)
                    <div class="col-sm-12 constructing_laft">
                        <img src="{{asset('images/training/'.$programme->photo)}}" alt="" class="img-block">
                        <a href="{{route('pages.training_mentorship_single',$programme->id)}}" style="color:#067000">{{$programme->name}}</a>
                        <p>{{$programme->content}}</p><br><br><hr>
                    </div>
                    @empty
                        
                    @endforelse
                </div>
            </div>
        </section>

@endsection