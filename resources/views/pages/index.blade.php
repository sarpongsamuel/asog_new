@extends('layouts.main')
@section('content')
<!-- Slider area -->
<section class="slider_area slider_area_tow row m0">
    <div class="slider_inner">
        @forelse ($slider as $slide)
        <div data-thumb="{{asset('images/slide/'. $slide->image)}}" data-src="{{asset('images/slide/'. $slide->image)}}">
            <div class="camera_caption">
                <div class="container">
                    <h5 class=" wow fadeInUp animated"></h5>
                    <h3 class=" wow fadeInUp animated" data-wow-delay="0.5s">{{$slide->header}}</h3>
                    <p class=" wow fadeInUp animated" data-wow-delay="0.8s">{{$slide->context}}</p>
                    {{-- <a class=" wow fadeInUp animated" data-wow-delay="1s" href="{{route('pages.events')}}">Read
                    More</a> --}}
                </div>
            </div>
        </div>
        @empty

        @endforelse
    </div>
</section>
<!-- End Slider area -->
<!-- What ew offer Area -->
<section class="what_we_area row">
    <div class="container">
        <div class="tittle wow fadeInUp">
            <h2></h2>
        </div>
        <div class="row construction_iner">
            @if (!collect($event)->isEmpty())
            <div class="col-md-4 col-sm-6 construction animated wow fade fadeInRight" data-wow-delay="0.5s">
                <div class="bor">
                    <div class="cns-content">
                        <i class="fa fa-newspaper-o" aria-hidden="true"></i>
                        <a href="{{route('pages.about')}}">Our Mission</a>
                        <p>
                            {!! substr($event->content, 0, 150)!!}{!! strlen($event->content )> 150 ?"...":"" !!}</p>

                        <a class="btn btn-outline-success btn-md" href="{{route('pages.events')}}">Read More</a>
                    </div>
                </div>
            </div>
            @endif
            @if (!collect($mission)->isEmpty())
            <div class="col-md-4 col-sm-6 construction animated wow fade fadeInRight" data-wow-delay="0.5s">
                <div class="bor">
                    <div class="cns-content">
                        <i class="fa fa-gift" aria-hidden="true"></i>
                        <a href="{{route('pages.about')}}">Our Mission</a>
                        <p>
                            {!! substr($mission->context, 0, 150)!!}{!! strlen($mission->context )> 150 ?"...":"" !!}</p>

                        <a class="btn btn-outline-success btn-md" href="{{route('pages.about')}}">Read More</a>
                    </div>
                </div>
            </div>
            @endif
            @if (!collect($programme)->isEmpty())
            <div class="col-md-4 col-sm-6 construction animated wow fade fadeInUp" data-wow-delay="0.5s">
                <div class="bor">
                    <div class="cns-content">
                        <i class="fa fa-pencil" aria-hidden="true"></i>
                        <a href="{{route('pages.training_mentorship')}}">Training & Mentorship</a>

                        <p>
                            {!! substr($programme->content, 0, 150) !!}{!! strlen($programme->content )> 150 ?"...":"" !!}</p>

                        <a class="btn btn-outline-success btn-md " href="{{route('pages.training_mentorship')}}">Read
                            More</a>

                    </div>
                </div>
            </div>
            @endif
        </div>
    </div>
</section>
<!-- End What ew offer Area -->

{{-- <section class="our_team_area">
            <div class="container">
                <div class="tittle wow fadeInUp">
                    <h2>Our Executives</h2>
                </div>
                <div class="row team_row">
                    <div class="col-md-3 col-sm-6 wow fadeInUp">
                       <div class="team_membar">
                            <img src="" alt="">
                            <div class="team_content">
                                <ul>
                                    <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                </ul>
                                <a href="#" class="name">Prodip Ghosh</a>
                                <h6>Founder &amp; CEO</h6>
                            </div>
                       </div>
                    </div>
                    <div class="col-md-3 col-sm-6 wow fadeInUp" data-wow-delay="0.2s">
                       <div class="team_membar">
                            <img src="{{asset('home/images/team/tm-2.jpg')}}" alt="">
<div class="team_content">
    <ul>
        <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
        <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
        <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
    </ul>
    <a href="#" class="name">Emran Khan</a>
    <h6>Web-Developer</h6>
</div>
</div>
</div>
<div class="col-md-3 col-sm-6 wow fadeInUp" data-wow-delay="0.3s">
    <div class="team_membar">
        <img src="{{asset('home/images/team/tm-2.jpg')}}" alt="">
        <div class="team_content">
            <ul>
                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
            </ul>
            <a href="#" class="name">Prodip Ghosh</a>
            <h6>Founder &amp; CEO</h6>
        </div>
    </div>
</div>
<div class="col-md-3 col-sm-6 wow fadeInUp" data-wow-delay="0.4s">
    <div class="team_membar">
        <img src="{{asset('home/images/team/tm-4.jpg')}}" alt="">
        <div class="team_content">
            <ul>
                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
            </ul>
            <a href="#" class="name">Jakaria Khan</a>
            <h6>Founder &amp; CEO</h6>
        </div>
    </div>
</div>
</div>
</div>
</section> --}}
<!-- End Our Team Area -->


<!-- Our Team Area -->


<!-- Our Achievments Area -->
{{-- <section class="our_achievments_area" data-stellar-background-ratio="0.3">
            <div class="container">
                <div class="tittle wow fadeInUp">
                    <h2>Our Achievments</h2>
                    <h4>Lorem Ipsum is simply dummy text of the printing and typesetting industry</h4>
                </div>
                <div class="achievments_row row">
                    <div class="col-md-3 col-sm-6 p0 completed">
                        <i class="fa fa-connectdevelop" aria-hidden="true"></i>
                        <span class="counter">800</span>
                        <h6>PROJECT COMPLETED</h6>
                    </div>
                    <div class="col-md-3 col-sm-6 p0 completed">
                        <i class="fa fa-home" aria-hidden="true"></i>
                        <span class="counter">230</span>
                        <h6>HOUSE RENOVATIONS</h6>
                    </div>
                    <div class="col-md-3 col-sm-6 p0 completed">
                        <i class="fa fa-child" aria-hidden="true"></i>
                        <span class="counter">1390</span>
                        <h6>WORKERS EMPLOYED</h6>
                    </div>
                    <div class="col-md-3 col-sm-6 p0 completed">
                        <i class="fa fa-trophy" aria-hidden="true"></i>
                        <span class="counter">125</span>
                        <h6>AWARDS WON</h6>
                    </div>
                </div>
            </div>
        </section> --}}
<!-- End Our Achievments Area -->





{{-- <!-- Our Latest Blog Area -->
        <section class="latest_blog_area">
            <div class="container">
                <div class="tittle wow fadeInUp">
                    <h2>Our Proud Sponsors</h2>
                </div>
                <div class="row latest_blog">
                    <div class="col-md-4 col-sm-6 blog_content">
                        <img src="{{asset('home/images/blog/lb-1.jpg')}}" alt="">
<a href="#" class="blog_heading">Our Latest Project</a>
<h4><small>by </small><a href="#">Emran Khan</a><span>/</span><small>ON </small> October 15, 2016</h4>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam sagittis iaculis velit in tristique. Curabitur ac urna
    urna. Sed sollicitudin at nisi sed accumsan... <a href="#">Read More</a></p>
</div>
<div class="col-md-4 col-sm-6 blog_content">
    <img src="{{asset('home/images/blog/lb-3.jpg')}}" alt="">
    <a href="#" class="blog_heading">Our Latest Project</a>
    <h4><small>by </small><a href="#">Prodip Ghosh</a><span>/</span><small>ON </small> October 15, 2016</h4>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam sagittis iaculis velit in tristique. Curabitur ac
        urna urna. Sed sollicitudin at nisi sed accumsan... <a href="#">Read More</a></p>
</div>
<div class="col-md-4 col-sm-6 blog_content">
    <img src="{{asset('home/images/blog/lb-2.jpg')}}" alt="">
    <a href="#" class="blog_heading">Our Latest Project</a>
    <h4><small>by </small><a href="#">Prodip Ghosh</a><span>/</span><small>ON </small> October 15, 2016</h4>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam sagittis iaculis velit in tristique. Curabitur ac
        urna urna. Sed sollicitudin at nisi sed accumsan... <a href="#">Read More</a></p>
</div>
</div>
</div>
</section>
<!-- End Our Latest Blog Area --> --}}

<!-- Our Partners Area -->
<section class="our_partners_area">
    <div class="container">
        <div class="tittle wow fadeInUp">
            <h2>Our Proud Sponsors</h2>
            <h4>

            </h4>
        </div>
        <div class="partners">
            @forelse ($sponsors as $sponsor)
            <div class="item">
                <img src="{{ asset('images/sponsor/' . $sponsor->logo) }}" alt="{{$sponsor->name}}" class="img-responsive">
            </div>

            @empty

            @endforelse
        </div>
    </div>
    <div class="book_now_aera">
        <div class="container">
            <div class="row book_now wow animated shake">
                <div class="col-md-10 booking_text">
                    <h4>Become Our Proud Sponsor Today</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                        labore et dolore magna aliqua.</p>
                </div>
                <div class="col-md-2 p0 book_bottun">
                    <a href="{{route('pages.sponsors')}}"><button class="butt">Become A Sponsor</button></a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Our Partners Area -->

@endsection