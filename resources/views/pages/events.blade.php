@extends('layouts.main')

@section('content')
<!-- Banner area -->
<section class="banner_area" data-stellar-background-ratio="0.5">
    <h2>News & Events</h2>
    <ol class="breadcrumb">
        <li><a href="{{route('pages.index')}}">Home</a></li>
        <li><a href="{{route('pages.events')}}" class="active">Event</a></li>
    </ol>
</section>
<!-- End Banner area -->
<section class="about_us_area about_us_2 row">
    <div class="container p-b-100">
        {{-- single event --}}
        @forelse ($events as $event)

        <div class="row about_row about_us2_pages">
            <div class="who_we_area col-md-5 animated wow fadeInLeft" data-wow-duration="2s">
                <a href="{{route('pages.event-details', $event->id)}}">
                    <img src="{{ asset('images/event/' . $event->photo) }}" alt="" class="img-responsives"
                        style="width:100%; height:250px">
                </a>
            </div>
            <div class="who_we_area col-md-7 animated wow fadeInRight" data-wow-duration="2s">
                <div class="subtittle">
                    <h4 class="text-left m-t-15">
                        <a href="{{ route('pages.event-details', $event->id) }}"
                            style="color:#f00000;">{{$event->name}}</a>
                    </h4>
                </div>
                <div>{!! substr($event->content, 0, 300)!!}{{ strlen($event->content )> 300 ?"...":"" }}</div>
                <span class="fa fa-calendar" style="color:#f00000"> Date
                    {{Carbon\Carbon::parse($event->event_date)->formatLocalized('%d, %B %Y')}} </span>
            </div>
        </div>
        @empty

        @endforelse
        {{-- end of single event --}}
        {{-- paginate here --}}
        {{$events->links()}}
    </div>
</section>
<!-- End About Us Area -->

@endsection