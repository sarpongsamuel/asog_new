@extends('layouts.main')
@section('content')
<!-- Banner area -->
<section class="banner_area" data-stellar-background-ratio="0.5">
    <h2>About</h2>
    <ol class="breadcrumb">
        <li><a href="{{route('pages.index')}}">Home</a></li>
        <li><a href="{{route('pages.about')}}" class="active">About</a></li>
    </ol>
</section>
<!-- End Banner area -->
<section class="our_team_area">
    <div class="container">
        <div class="tittle wow fadeInUp">
            <h2>Presidential Address</h2>
        </div>
        {{-- <div class="row team_row">
                        <div class="col-md-3 col-sm-3 wow fadeInUp">
                           <img class="img-responsive img-thumbnail img-circle" src="{{asset('main/images/speaker_6.jpg')}}"
        alt="" style="height:150px; width:150px" >
    </div>
    <div class="col-md-8 col-sm-8 wow fadeInUp offset-1">
        <p class="text">
            Lorem Ipsum is simply dummy text of the printing and typesetting industry.
            Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
            when an unknown printer took a galley of type and scrambled it to make a type specimen book.
            It has survived not only five centuries, but also the leap into electronic typesetting, remaining
            essentially unchanged.
            It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and
            more recently with
        </p>
    </div>
    </div>
    <hr> --}}
    </div>
</section>
{{-- our mission --}}
<section class="our_team_area">
    <div class="container">
        <div class="tittle wow fadeInUp">
            <h2>Our Mission</h2><br>
        </div>
        @if (!collect($mission)->isEmpty())
        <div class="row team_row">
            <div class="col-md-12 col-sm-12 wow fadeInUp ">
                <p class="text">
                    {{$mission->context}}
                </p>
            </div><br>
        </div>
        @endif
        <hr><br>
        <br>
        <div class="row">
            <div class="col text-center">
                @if (!collect($constitution)->isEmpty())
                <a class="btn butt" href="{{asset('storage/doc/'.$constitution->file)}}"><span
                        class="fa fa-download"></span> Constitution</a>
                @endif
            </div>
        </div>
    </div>
</section>



@endsection