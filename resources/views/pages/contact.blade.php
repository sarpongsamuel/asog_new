@extends('layouts.main')
@section('content')
@push('extra_styles')
{{ Html::style('dash/css/parsley.css') }}
@endpush
<!-- Banner area -->
<section class="banner_area" data-stellar-background-ratio="0.5">
    <h2>Contact</h2>
    <ol class="breadcrumb">
        <li><a href="{{route('pages.index')}}">Home</a></li>
        <li><a href="{{route('pages.contact')}}" class="active">Contact</a></li>
    </ol>
</section>
<!-- Map -->
<div class="contact_map">
    <iframe
        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3962.777161628924!2d-1.5738054493852303!3d6.674508823216795!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xfdb9461ea6fb9af%3A0xb0da4f6d9c4b361f!2sKwame+Nkrumah+University+of+Science+and+Technology+(KNUST)!5e0!3m2!1sen!2sgh!4v1562946417206!5m2!1sen!2sgh"
        width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>
<!-- End Map -->

<!-- All contact Info -->
<section class="all_contact_info">
    <div class="container">
        <div class="row contact_row">
            <div class="col-sm-6 contact_info">
                <h2>Contact Info</h2>
                <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered
                    alteration in some form, by injected humour, or randomised words which don't look even slightly
                    believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't
                    anything embarrassing hidden in the middle of text.</p>
                <div class="location">
                    <div class="location_laft">


                        <a href="#">email</a>
                    </div>
                    <div class="address">
                        <a href="#"></a>

                        <a href="mailto:">asoggh19@gmail.com</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 contact_info send_message">
                <h2>Send Us a Message</h2>
                {{-- <form class="form-inline contact_box"> --}}
                {{Form::open(['route'=>'contact.store', 'class'=>'form-inline contact_box', 'data-parsley-validate' => '',])}}
                {{ Form::text('name', null, ['class' => 'form-control input_box', 'required'=>''])}}
                {{ Form::email('email', null, ['class' => 'form-control input_box', 'required'=>''])}}
                {{ Form::textarea('message', null, ['class' => 'form-control input_box', 'required'=>'', 'rows'=>'7'])}}
                <br><br>
                {{ Form::submit('Send Message', ['class' => 'form-control btn btn-default'])}}
                {{Form::close()}}

            </div>
        </div>
    </div>
</section>
<!-- End All contact Info -->


@endsection
@push('scripts')
{{ Html::script('dash/js/parsley.min.js') }}
@endpush