@extends('layouts.main')
@section('content')
<!-- Banner area -->
<section class="banner_area" data-stellar-background-ratio="0.5">
    <h2>Gallery</h2>
    <ol class="breadcrumb">
        <li><a href="{{route('pages.index')}}">Home</a></li>
        <li><a href="{{route('pages.galleries')}}" class="active">Gallery</a></li>
    </ol>
</section>
<!-- Our Featured Works Area -->
<div class="container">
    <section class="featured_works row" data-stellar-background-ratio="0.2">
        <div class="featured_gallery">
            @forelse ($images as $image)
            <div class="col-md-4 col-sm-4 col-xs-6 gallery_iner p0">
                <img src="{{asset('images/gallery/'.$image->image)}}" alt="" style="padding:2px; height:">
                <div class="gallery_hover">
                    <h4>{{$image->name}}</h4>
                    <a href="{{asset('images/gallery/'.$image->image)}}" data-lightbox="image-1"
                        data-title="{{$image->name}}">view</a>
                </div>
            </div>
            @empty
            <h3 class="text-center">No gallery image</h3>
            @endforelse
        </div>
    </section>
    <section class="row">
        <div class="col-sm-6 text-center">
            {{$images->links()}}
        </div>
        <div class="col-sm-6 text-right m-t-25">
            @if ($images->count() > 0 && ( ($images->currentPage() * $images->perPage() )== $images->total()))
            <a href="https://www.google.com/drive/" target="new" class="btn btn-sm btn-success">
                <i class="fa fa-camera"> View More Photos</i>
            </a>
            @endif
        </div>
    </section>
</div><br>
<!-- End Our Featured Works Area -->



@endsection