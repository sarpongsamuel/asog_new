@extends('layouts.main')

@section('content')
<!-- Banner area -->
<section class="banner_area" data-stellar-background-ratio="0.5">
    <h2>News & Events</h2>
    <ol class="breadcrumb">
        <li><a href="{{route('pages.index')}}">Home</a></li>
        <li><a href="{{route('pages.events')}}" class="active">Event</a></li>
    </ol>
</section>
<!-- End Banner area -->
<!-- blog area -->
<section class="blog_all">
    <div class="container">
        @include('partials._message')

        <div class="row m0 blog_row">
            <div class="col-sm-8 main_blog wow animated fadeInUp" data-wow-delay="1s">
                <img src="{{ asset('images/event/' . $event->photo) }}" alt="" style="width:100%; height:300px"
                    class="img-block">
                <div class="col-xs-1 p0">
                    <div class="blog_date">
                        <a href="#">{{Carbon\Carbon::parse($event->event_date)->formatLocalized('%d')}}</a>
                        <a href="#">{{ substr(Carbon\Carbon::parse($event->event_date)->formatLocalized('%B'),0,3)}}</a>
                    </div>
                </div>
                <div class="col-xs-11 blog_content">
                    <a class="blog_heading" href="#">{{$event->name}}</a>
                    <a class="blog_admin" href="#"><i class="fa fa-user" aria-hidden="true"></i>{{$event->host}}</a>
                    <ul class="like_share">
                        <li><a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>{{$event->event_venue}}</a>
                        </li>
                        <li><a href="#"><i class="fa fa-calendar" aria-hidden="true"></i>
                                {{Carbon\Carbon::parse($event->created_at)->formatLocalized('%d, %B %Y')}}</a>
                        </li>
                        {{-- <li><a href="#"><i class="fa fa-share-alt" aria-hidden="true"></i></a></li> --}}
                    </ul>
                    <p>{!!$event->content!!}</p>
                </div>
                @if ($event->registration_ongoing)
                <div class="col-xs-12">
                    <a href="{{ route('pages.event_register.create', $event->name)}}"
                        class="btn btn-success pull-right">Register</a>
                </div>
                @endif

            </div>
            {{-- <div class="col-sm-4 widget_area wow animated fadeInRight" data-wow-delay="1s"> --}}
            {{-- <div class="resent"> --}}
            {{-- <h3>RECENT POSTS</h3> --}}

            {{-- @forelse ($widgets as $widget)
                    <div class="media">
                            <div class="media-left">
                                <a href="#">
                                    <img class="media-object" src="{{asset('event/small/p.png')}}" alt=""
            style="height:25px">
            </a>
        </div>
        <div class="media-body">
            <a href="">Get informed about construction industry trends &amp; development.</a>
            <h6>Oct 19, 2016</h6>
        </div>
    </div>
    @empty

    @endforelse --}}
    {{-- </div> --}}
    {{-- <div class="search">
                        <input type="search" name="search" class="form-control" placeholder="Search">
                    </div> --}}
    {{-- </div> --}}
    </div>
    </div>
</section>
<!-- End blog area -->

<!-- End blog area -->

@endsection