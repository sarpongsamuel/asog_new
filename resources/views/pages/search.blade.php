@extends('layouts.main')
@section('content')
<!-- Banner area -->
<section class="banner_area" data-stellar-background-ratio="0.5">
    <h2>Search Result</h2>
    <ol class="breadcrumb">
        <li><a href="{{route('pages.index')}}">Home</a></li>
        {{-- <li><a href="{{route('pages.about')}}" class="active">About</a></li> --}}
    </ol>
</section>
<!-- End Banner area -->
<section class="our_team_area">
    <div class="container">
        <div class="tittle wow fadeInUp">
            <h2>{{$publication->count()}} result(s) for '{{request()->input('query')}}'</h2><br><br>
        </div>

        @forelse ($publication as $item)


        <div class="row tab">
            <div class="col-md-2">
                <img src="{{ asset('publication/pdf.png') }}" alt="" class="img-responsive"
                    style="height:70px; margin-top:40px; margin-left:40px;"><br>
                <a href="{{asset('storage/publication/'.$item->document)}}">
                    <span class="text-center" style="margin-left:35px">
                        <i class="fa fa-cloud-download"></i>
                        download
                    </span>
                </a>

            </div>
            <div class="col-md-10">
                <div class="row">
                    <div class="col-md-12 r1"><b>Author(s):</b> {{$item->author}}</div>
                    <hr style="border:1px solid #dcdcdc;">
                    <div class="col-md-12 r1"><b>Year Of Publication:</b> {{$item->publication_date}}</div>
                    <hr style="border:1px solid #dcdcdc;">
                    <div class="col-md-12 r1"><b>Title: </b>{{$item->title}}</div>
                    <hr>

                </div>
            </div>
        </div>
        <hr style="">
        @empty

        @endforelse

    </div>
</section>
{{-- our mission --}}




@endsection