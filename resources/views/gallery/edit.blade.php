@extends('Layouts.dashboard')
@section('title', '|Gallery-Create')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">Add Gallery Image</div>
            <div class="card-body">
                <div class="card-title">
                    <h3 class="text-center title-2">New Gallery Image</h3>
                </div>
                <hr>
                {!! Form::model($gallery, ['route' => ['gallery.update',$gallery->id],'data-parsley-validate' => '',
                'files'=>true, 'method' => 'PUT']) !!}
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            {{ Form::label('name', 'Image Name', ['class' => 'control-label mb-1']) }}
                            {{ Form::text('name', null, ['class' => 'form-control', 'required'=>''])}}
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            {{ Form::label('photo', 'Photo', ['class' => 'control-label mb-1']) }} <br>
                            {{ Form::file('photo', null, ['class' => 'form-control', 'required'=>''])}}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <a href="{{route('gallery.index')}}" class="btn btn-danger btn-block">Cancel</a>
                    </div>
                    <div class="col-md-6">
                        {!! Form::submit('Update Image', array('class' => 'btn btn-success btn-block',
                        'id'=>'btnSubmit')) !!}
                    </div>
                </div>


                {{Form::close()}}
            </div>
        </div>
    </div>

</div>

@endsection