@extends('Layouts.dashboard')
@section('title', '| Gallery')

@section('content')
<div class="row">
    <div class="col-md-12">
        <!-- DATA TABLE -->
        @include('partials._message')
        <br>
        <h3 class="title-5 m-b-35">All Events</h3>
        <div class="table-data__tool">
            <div class="table-data__tool-left">
                <button class="au-btn-filter">
                    <i class="zmdi zmdi-filter-list"></i>filters</button>
            </div>
            <div class="table-data__tool-right">
                <a href="{{route('gallery.create')}}"><button class="au-btn au-btn-icon au-btn--green au-btn--small">
                        <i class="zmdi zmdi-plus"></i>add Image</button></a>
            </div>
        </div>
        <div class="table-responsive table-responsive-data2">
            <table class="table table-data2">
                <thead>
                    <tr>
                        <th>
                            <label class="au-checkbox">
                                <input type="checkbox">
                                <span class="au-checkmark"></span>
                            </label>
                        </th>
                        <th>Photo</th>
                        <th>Title/Name</th>

                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($image as $img)

                    <tr class="tr-shadow">
                        <td>
                            <label class="au-checkbox">
                                <input type="checkbox">
                                <span class="au-checkmark"></span>
                            </label>
                        </td>
                        <td><img src="{{ asset('images/gallery/' . $img->image) }}" alt="" class="img-responsive"
                                style="height:100px"></td>
                        <td>
                            <span class="block-email">{{$img->name}}</span>
                        </td>
                        <td>
                            <div class="table-data-feature">
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                    <a href="{{route('gallery.edit',$img->id)}}"><i class="zmdi zmdi-edit"
                                            style="color:green"></i></a>
                                </button>
                                {{Form::open(['route'=>['gallery.destroy',$img->id], 'method'=>'delete'])}}
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                    <i class="zmdi zmdi-delete" style="color:red"></i>
                                </button>
                                {{Form::close()}}

                            </div>
                        </td>
                    </tr>
                    <tr class="spacer"></tr>
                    @empty

                    @endforelse

                </tbody>
            </table>
        </div>
        <!-- END DATA TABLE -->
    </div>
</div>

@endsection