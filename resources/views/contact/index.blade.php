@extends('Layouts.dashboard')
@section('title', '| Messages')

@section('content')
<div class="col-lg-12">
    <!-- USER DATA-->
    @include('partials._message')
    <div class="user-data m-b-30">
        <h3 class="title-3 m-b-30">
            <i class="zmdi zmdi-envelope"></i>
            Messages
        </h3>
        <div class="table-responsive table-data">
            <table class="table">
                <thead>
                    <tr>
                        <td>
                            <label class="au-checkbox">
                                <input type="checkbox">
                                <span class="au-checkmark"></span>
                            </label>
                        </td>
                        <td><b>Name</b></td>
                        <td><b>Email</b></td>
                        <td><b>Message</b></td>
                        <td></td>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($messages as $message)
                    <tr>
                        <td>
                            <label class="au-checkbox">
                                <input type="checkbox">
                                <span class="au-checkmark"></span>
                            </label>
                        </td>
                        <td>
                            <div class="table-data__info">
                                <h6>{{$message->name}}</h6>
                            </div>
                        </td>
                        <td>
                            <div class="table-data__info">
                                <h6>{{$message->email}}</h6>
                            </div>
                        </td>
                        <td>
                            <div class="table-data__info">
                                <h6>{{ substr($message->message, 0, 30)}}{{ strlen($message->message )> 50 ?"...":"" }}
                                </h6>
                            </div>
                        </td>
                        <td>
                        <td>
                            <div class="table-data-feature">
                                <a href="{{route('contact.show', $message->id)}}"><button class="item" type="submit"
                                        data-toggle="tooltip" data-placement="top" title="view">
                                        <i class="zmdi zmdi-eye" style="color:blue"></i>
                                    </button><a>

                                        {{Form::Open(['route'=>['contact.destroy',$message->id], 'method'=>'delete'])}}
                                        <button class="item" type="submit" data-toggle="tooltip" data-placement="top"
                                            title="Delete">
                                            <i class="zmdi zmdi-delete" style="color:red"></i>
                                        </button>
                                        {{Form::close()}}
                            </div>
                        </td>

                        </td>
                    </tr>
                    @empty
                    @endforelse


                </tbody>
            </table>
        </div>

    </div>
    <!-- END USER DATA-->
</div>

@endsection