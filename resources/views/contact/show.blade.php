@extends('Layouts.dashboard')
@section('title', '|Gallery-Create')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">View Message</div>
            <div class="card-body">
                <div class="card-title">
                    <h3 class="text-center title-2">View Message</h3>
                </div>
                <hr>
                {!! Form::model($message) !!}
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            {{ Form::label('name', 'Sender Name', ['class' => 'control-label mb-1']) }}
                            {{ Form::text('name', null, ['class' => 'form-control', 'required'=>'','disabled'=>true])}}
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            {{ Form::label('email', 'Sender Email', ['class' => 'control-label mb-1']) }} <br>
                            {{ Form::text('email', null, ['class' => 'form-control', 'required'=>'','disabled'=>true])}}
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            {{ Form::label('message', 'Company Message', ['class' => 'control-label mb-1']) }} <br>
                            {{ Form::textarea('message', null, ['class' => 'form-control', 'required'=>'', 'rows'=>'10' ,'disabled'=>true])}}
                        </div>
                    </div>
                </div>
                <a href="{{route('contact.index')}}" class="btn btn-danger btn-block">Cancel</a>

                {{-- {!! Form::submit('update Sponsor', array('class' => 'btn btn-success btn-block', 'id'=>'btnSubmit')) !!}  --}}
                {{Form::close()}}
            </div>
        </div>
    </div>

</div>

@endsection