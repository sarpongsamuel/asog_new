@extends('Layouts.dashboard')
@section('title', '| Publication')
@push('styles')
{{ Html::style('dash/css/parsley.css') }}
@endpush
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">Add New Publication</div>
            <div class="card-body">
                <div class="card-title">
                    <h3 class="text-center title-2">New Publication</h3>
                </div>
                <hr>
                {!! Form::open(['route' => 'abstractresearch.store', 'data-parsley-validate' => '', 'files'=>true]) !!}

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('conference_year', 'Conference Year', ['class' => 'control-label mb-1']) }}
                            {{ Form::text('conference_year', null, ['class' => 'form-control', 'required'=>''])}}
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('document', 'Document', ['class' => 'control-label mb-1']) }}
                            {{Form::select('publication_type', ['Abstract' => 'Abstract', 'Research Proposal' => 'Research Proposal'],null,['class' => 'form-control mb-1'])}}
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('document', 'Document', ['class' => 'control-label mb-1']) }}
                            {{ Form::file('document', null, ['class' => 'form-control', 'required'=>''])}}
                        </div>
                    </div>


                </div>
                {!! Form::submit('Add To document', array('class' => 'btn btn-success btn-block', 'id'=>'btnSubmit'))
                !!}
                {{Form::close()}}
            </div>
        </div>
    </div>

</div>

@endsection
@push('scripts')
{{ Html::script('dash/js/parsley.min.js') }}
@endpush