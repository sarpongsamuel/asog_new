@extends('Layouts.dashboard')
@section('title', '| Abstracst & Research')

@section('content')
<div class="col-lg-12">
    <!-- USER DATA-->
    @if (Session::has('success'))
    <div class="alert au-alert-success alert-dismissible fade show au-alert au-alert--100per" role="alert">
        <i class="zmdi zmdi-check-circle"></i>
        <span class="content"><b></b> : {{Session::get('success')}}</span>
    </div>
    @endif
    <div class="user-data m-b-30">
        <h3 class="title-3 m-b-30">
            <a href="{{route('abstractresearch.create')}}" class="btn btn-primary btn-right"> <span
                    class="fa fa-plus"></span> Abstract / Research</a></h3>

        <div class="table-responsive table-data">
            <table class="table">
                <thead>
                    <tr>
                        <td>
                            <label class="au-checkbox">
                                <input type="checkbox">
                                <span class="au-checkmark"></span>
                            </label>
                        </td>
                        <td><b></b></td>
                        <td><b>Conference Year</b></td>
                        <td><b>Type</b></td>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($docs as $doc)
                    <tr>
                        <td>
                            <label class="au-checkbox">
                                <input type="checkbox">
                                <span class="au-checkmark"></span>
                            </label>
                        </td>
                        <td>
                            <div class="table-data__info">
                                <h6><img src="{{ asset('publication/pdf.png') }}" alt="" style="height:30px"></h6>
                            </div>
                        </td>
                        <td>
                            <div class="table-data__info">
                                <h6>{{ $doc->conference_year }}
                                </h6>
                            </div>
                        </td>
                        <td>
                            <div class="table-data__info">
                                <h6>{{ $doc->publication_type }}
                                </h6>
                            </div>
                        </td>

                        <td>
                            <div class="table-data-feature">

                                {{Form::Open(['route'=>['abstractresearch.destroy',$doc->id], 'method'=>'delete'])}}
                                <button class="item" type="submit" data-toggle="tooltip" data-placement="top"
                                    title="Delete">
                                    <i class="zmdi zmdi-delete" style="color:red"></i>
                                </button>
                                {{Form::close()}}

                            </div>
                        </td>
                    </tr>
                    @empty
                    @endforelse


                </tbody>
            </table>
        </div>

    </div>
    <!-- END USER DATA-->
</div>

@endsection