
	<footer class="footer">
		<div class="footer_container">
			<div class="container">
				<div class="row">
					<div class="col-lg-2 footer_col">
					</div>
					<!-- Footer About -->
					<div class="col-lg-8 footer_col">
						<h3 style="color:white;text-align:center">ASOG</h3>
						
						<div class="footer_column" style="margin-top:-40px">
							<div class="gallery d-flex flex-row align-items-start justify-content-between flex-wrap">
								<div class="gallery_item text-center">
									<a class="colorbox" href="{{asset('main/images/gallery_1_large.jpg')}}">
										<img src="{{asset('main/images/logo/asoglogo.png')}}" alt="" height="100px" width="120px" class="mx-auto d-block">
									</a>
								</div>
							</div>
						</div>
					</div>

			
					
					<!-- Gallery -->
					<div class="col-lg-2 footer_col">
						
					</div>

				</div>
			</div>
		</div>
		<div class="footer_bar">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="footer_bar_content d-flex flex-row align-items-center justify-content-start">
							<div class="copyright"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>
