<footer class="footer_area">
    <div class="container">
        <div class="footer_row row">
            <div class="col-md-4 col-sm-6 footer_about">
                <h2>ANATOMY SOCIETY OF GHANA</h2>
                {{-- <img src="images/footer-logo.png" alt=""> --}}
                {{-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p> --}}
                <span><img src="{{asset('main/images/logo/asog.png')}}" alt="" class="img-responsives" height="120px"
                        style="margin-left:30px"></span>
                {{-- <ul class="socail_icon">
                    <li><a href="#">
                    </a></li>
                </ul> --}}
            </div>
            <div class="col-md-4 col-sm-6 footer_about quick">
                <h2>Quick links</h2>
                <ul class="quick_link">
                    <li><a href="{{ route('pages.training_mentorship')}}"><i style="color:red"
                                class="fa fa-chevron-right"></i>Training & Mentorship</a></li>
                    <li><a href="{{ route('pages.events')}}"><i style="color:red" class="fa fa-chevron-right"></i>News &
                            Events</a></li>
                    <li><a href="{{ route('pages.sponsors')}}"><i style="color:red"
                                class="fa fa-chevron-right"></i>Sponsorship</a></li>
                    <li><a href="{{ route('pages.about')}}"><i style="color:red" class="fa fa-chevron-right"></i>About
                            Us</a></li>
                    <li><a href="{{ route('pages.contact')}}"><i style="color:red"
                                class="fa fa-chevron-right"></i>Contact Us</a></li>
                </ul>
            </div>

            <div class="col-md-4 col-sm-6 footer_about">
                <h2>CONTACT US</h2>
                <address>
                    <p>Have questions, comments or just want to say hello:</p>
                    <ul class="my_address">
                        <li><a href="#"><i style="color:red" class="fa fa-envelope"
                                    aria-hidden="true"></i>asoggh19@gmail.com</a></li>
                        <li><a href="#"><i style="color:red" class="fa fa-phone" aria-hidden="true"></i>+233
                                243947500</a></li>
                        <li><a href="#"><i style="color:red" class="fa fa-map-marker" aria-hidden="true"></i><span>Ghana
                                </span></a></li>
                    </ul>
                </address>
            </div>
        </div>
    </div>
    <div class="copyright_area">
        Copyright &copy; 2019 All rights reserved.</a>
    </div>
</footer>