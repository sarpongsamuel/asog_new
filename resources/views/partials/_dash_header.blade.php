<header class="header-desktop3 d-none d-lg-block">
    <div class="section__content section__content--p35">
        <div class="header3-wrap">
            <div class="header__logo">
                <a href="#">
                    <img src="{{asset('main/images/logo/asoglogo.png')}}" alt="Asog" class="img-responsive"
                        height="20px" /><span style="color:white"> ASOG</span>
                </a>
            </div>
            <div class="header__navbar">
                <ul class="list-unstyled">
                    <li class="has-sub">
                        <a href="{{route('dashboard.index')}}">
                            <i class="fas fa-tachometer-alt"></i>Dashboard
                            <span class="bot-line"></span>
                        </a>
                    </li>
                    <li>
                        <a href="{{route('gallery.index')}}">
                            <i class="fas fa-photo"></i>
                            <span class="bot-line"></span>Gallery</a>
                    </li>
                    <li>
                        <a href="{{route('event.index')}}">
                            <i class="fas fa-trophy"></i>
                            <span class="bot-line"></span>Events</a>
                    </li>
                    <li class="has-sub">
                        <a href="{{route('sponsor.index')}}">
                            <i class="fas fa-list"></i>
                            <span class="bot-line"></span>Sponsors</a>
                    </li>
                    <li class="has-sub">
                        <a href="#">
                            <i class="fas fa-copy"></i>
                            <span class="bot-line"></span>Publications</a>
                        <ul class="header3-sub-list list-unstyled">
                            <li>
                                <a href="{{route('abstractresearch.index')}}">Abstract & Research</a>
                            </li>
                            <li>
                                <a href="{{route('publication.index')}}">Publications</a>
                            </li>

                            <li>
                                <a href="{{route('publicationCategory.index')}}">Publications Category</a>
                            </li>
                        </ul>
                    </li>
                    <li class="has-sub">
                        <a href="#">
                            <i class="fas fa-caret-down"></i>
                            <span class="bot-line"></span>More</a>
                        <ul class="header3-sub-list list-unstyled">
                            <li>
                                <a href="{{route('title.index')}}">Titles</a>
                            </li>

                            <li>
                                <a href="{{route('slider.index')}}">Home Slider</a>
                            </li>
                            <li>
                                <a href="{{route('training_mentorship.index')}}">Training And Mentorship</a>
                            </li>
                            <li>
                                <a href="{{route('constitution.index')}}">Our Constitution</a>
                            </li>
                            <li>
                                <a href="{{route('mission.index')}}">Our Mission</a>
                            </li>
                            <li>
                                <a href="{{route('contact.index')}}">Messages</a>
                            </li>
                            <li>
                                <a href="{{route('registration_code.index')}}">Registration Code</a>
                            </li>
                            <li>
                                <a href="{{route('pages.index')}}" target="new">Visit Website</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="header__tool">
                <a href="{{ route('account.index') }}">
                    <div class="header-button-item js-item-menu">
                        <i class="zmdi zmdi-account"></i>
                    </div>
                </a>
                {{-- logout button --}}
                <a href="{{ route('logout') }}" onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">
                    <div class="header-button-item js-item-menu">
                        <i class="zmdi zmdi-lock"></i>
                    </div>
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>

            </div>
        </div>
    </div>
</header>