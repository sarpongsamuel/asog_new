<section class="statistic statistic2">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-lg-3">
                <div class="statistic__item statistic__item--green">
                    <h2 class="number">{{$publication}}</h2>
                    <span class="desc">Articles & Publications</span>
                    <div class="icon">
                        <i class="zmdi zmdi-account-o"></i>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3">
                <div class="statistic__item statistic__item--orange">
                    <h2 class="number">{{$event}}</h2>
                    <span class="desc">Events</span>
                    <div class="icon">
                        <i class="zmdi zmdi-notifications-active animated infinite pulse zmdi-hc-fw mdc-text-blue"></i>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3">
                <div class="statistic__item statistic__item--blue">
                    <h2 class="number">{{$school}}</h2>
                    <span class="desc">Associate Schools</span>
                    <div class="icon">
                        <i class="zmdi zmdi-calendar-note animated"></i>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3">
                <div class="statistic__item statistic__item--red">
                    <h2 class="number">{{$training}}</h2>
                    <span class="desc">training & Mentorship</span>
                    <div class="icon">
                        <i class="zmdi zmdi-tag-o"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="statistic statistic2">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-lg-3">
                <div class="statistic__item statistic__item--blue">
                    <h2 class="number">{{$gallery}}</h2>
                    <span class="desc">Gallery</span>
                    <div class="icon">
                        <i class="zmdi zmdi-image-o fadeInLeft"></i>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3">
                <div class="statistic__item statistic__item--red">
                    <h2 class="number">{{$contact}}</h2>
                    <span class="desc">Messages</span>
                    <div class="icon">
                        <i class="zmdi zmdi-directions-bike animated infinite fadeInLeft zmdi-hc-fw"></i>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3">
                <div class="statistic__item statistic__item--blue">
                    <h2 class="number">{{$slider}}</h2>
                    <span class="desc">Home Slider</span>
                    <div class="icon">
                        <i class="zmdi zmdi-calendar-note"></i>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3">
                <div class="statistic__item statistic__item--green">
                    <h2 class="number">{{$sponsor}}</h2>
                    <span class="desc">Sponsors</span>
                    <div class="icon">
                        <i class="zmdi zmdi-money"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>