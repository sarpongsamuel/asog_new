<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Anatomical Society Of Ghana</title>

    <!-- Favicon -->
    <link rel="icon" href="images/favicon.png" type="image/x-icon" />
    <!-- Bootstrap CSS -->
    <link href="{{asset('home/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Animate CSS -->
    <link href="{{asset('home/vendors/animate/animate.css')}}" rel="stylesheet">
    <!-- Icon CSS-->
    <link rel="stylesheet" href="{{asset('home/vendors/font-awesome/css/font-awesome.min.css')}}">
    <!-- Camera Slider -->
    <link rel="stylesheet" href="{{asset('home/vendors/camera-slider/camera.css')}}">
    <!-- Owlcarousel CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('home/vendors/owl_carousel/owl.carousel.css')}}" media="all">
    <!-- lightbox CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('home/vendors/lightbox2/css/lightbox.css')}}">
    <!--Theme Styles CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('home/css/style.css')}}" media="all" />
    <!-- Util CSS -->
    <link href="{{asset('css/util.css')}}" rel="stylesheet">
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.min.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
    {{ Html::style('dash/css/parsley.css') }}

    @stack('extra_styles')
</head>