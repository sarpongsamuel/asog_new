<nav class="navbar navbar-default header_aera" id="main_navbar">
    <div class="container">
        <!-- searchForm -->
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="col-md-2 p0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#min_navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{route('pages.index')}}">
                    <img src="{{asset('slide/anat2.png')}}" alt="" class="img-responsive"
                        style="margin-top:-10px; height:50px;">
                </a>
            </div>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="col-md-10 p0">
            <div class="collapse navbar-collapse" id="min_navbar">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="{{route('pages.index')}}">Home</a></li>
                    <li><a href="{{route('pages.about')}}">About Us</a></li>
                    <li><a href="{{route('pages.events')}}">Events</a></li>
                    <li><a href="{{route('pages.galleries')}}">Gallery</a></li>
                    <li><a href="{{route('pages.publications')}}">Publications</a></li>
                    <li><a href="{{route('pages.training_mentorship')}}">Training & Mentorship</a></li>

                    {{-- <li class="dropdown submenu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Associate Schools <span
                                class="fa fa-caret-down"></span></a>
                        <ul class="dropdown-menu">
                            @foreach ($navSchools as $navSchool)
                            <li><a href="{{route('pages.assoc_schools', $navSchool->name)}}">{{$navSchool->name}}</a>
                    </li>
                    @endforeach
                </ul>
                </li> --}}
                {{-- <li class="dropdown submenu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">More <span
                            class="fa fa-caret-down"></span></a>
                    <ul class="dropdown-menu">

                    </ul>
                </li> --}}
                <li><a href="{{route('pages.contact')}}">Contact</a></li>
                <li><a href="{{route('pages.sponsors')}}" class="nav_but"><i class="fa fa-gift"></i> Sponsor</a>
                </li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div>
    </div><!-- /.container -->
</nav>
<!-- End Header_Area -->