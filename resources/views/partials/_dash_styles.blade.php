<!-- Fontfaces CSS-->
<link href="{{asset('dash/css/font-face.css')}}" rel="stylesheet" media="all">
<link href="{{asset('dash/vendor/font-awesome-4.7/css/font-awesome.min.css')}}" rel="stylesheet" media="all">
<link href="{{asset('dash/vendor/font-awesome-5/css/fontawesome-all.min.css')}}" rel="stylesheet" media="all">
<link href="{{asset('dash/vendor/mdi-font/css/material-design-iconic-font.min.css')}}" rel="stylesheet" media="all">

<!-- Bootstrap CSS-->
<link href="{{asset('dash/vendor/bootstrap-4.1/bootstrap.min.css')}}" rel="stylesheet" media="all">

<!-- Vendor CSS-->
{{-- <link href="{{asset('dash/vendor/animsition/animsition.min.css')}}" rel="stylesheet" media="all"> --}}
<link href="{{asset('dash/vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css')}}" rel="stylesheet"
    media="all">
<link href="{{asset('dash/vendor/wow/animate.css')}}" rel="stylesheet" media="all">
<link href="{{asset('dash/vendor/css-hamburgers/hamburgers.min.css')}}" rel="stylesheet" media="all">
<link href="{{asset('dash/vendor/slick/slick.css')}}" rel="stylesheet" media="all">
<link href="{{asset('dash/vendor/select2/select2.min.css')}}" rel="stylesheet" media="all">
<link href="{{asset('dash/vendor/perfect-scrollbar/perfect-scrollbar.css')}}" rel="stylesheet" media="all">

<!-- Main CSS-->
<link href="{{asset('dash/css/theme.css')}}" rel="stylesheet" media="all">

<script src="https://cdn.tiny.cloud/1/dapqw2yv8vfwcf10rairgl621b489f0i0gdxyj3h61nlt244/tinymce/5/tinymce.min.js">
</script>
<script>
    tinymce.init({
        selector: '#wysiwyg-textarea',
        plugins: "link",
        menubar: false,
        plugins: [
            'advlist autolink lists link image charmap print preview anchor textcolor',
            'searchreplace visualblocks code fullscreen',
            'insertdatetime media table paste code help wordcount'
        ],
        toolbar: 'undo redo | formatselect | bold italic backcolor link | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
    });
</script>
@stack('styles')