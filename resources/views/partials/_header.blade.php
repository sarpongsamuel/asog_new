<header class="header">
    <div class="header_content d-flex flex-row align-items-center justify-content-start">
        
        <!-- Logo -->
        <div class="logo">
            <a href="#">
            <img src="{{asset('main/images/logo/asoglogo.png')}}" alt="" class="img-responsive">
            </a>
        </div>

        <!-- Main Navigation -->
        @include('partials._nav')

        <div class="header_right ml-auto d-flex flex-row align-items-center justify-content-start">
            <div class="log_reg">
                <ul class="d-flex flex-row align-items-start justify-content-start">
                    <li><a href="#">Login</a></li>
                    <li><a href="#">Register</a></li>
                </ul>
            </div>
            <div class="social">
                <ul class="d-flex flex-row align-items-center justify-content-start">
                    <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                    <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                <li><a href="{{route('dashboard')}}"><i class="fa fa-user" aria-hidden="true"></i></a></li>

                </ul>
            </div>
            <div class="header_link">
            <a href="{{route('pages.sponsors')}}">
                    <span>Sponsor<img src="images/arrow.png" alt=""></span>
                </a>
            </div>
            <div class="hamburger"><i class="fa fa-bars" aria-hidden="true"></i></div>
        </div>
    </div>
</header>
