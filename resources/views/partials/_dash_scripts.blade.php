<!-- Jquery JS-->
    <script src="{{asset('dash/vendor/jquery-3.2.1.min.js')}}"></script>
    <!-- Bootstrap JS-->
    <script src="{{asset('dash/vendor/bootstrap-4.1/popper.min.js')}}"></script>
    <script src="{{asset('dash/vendor/bootstrap-4.1/bootstrap.min.js')}}"></script>
    <!-- Vendor       -->
    <script src="{{asset('dash/vendor/slick/slick.min.js')}}">
    </script>
    <script src="{{asset('dash/vendor/wow/wow.min.js')}}"></script>
    <script src="{{asset('dash/vendor/animsition/animsition.min.js')}}"></script>
    <script src="{{asset('dash/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js')}}">
    </script>
    <script src="{{asset('dash/vendor/counter-up/jquery.waypoints.min.js')}}"></script>
    <script src="{{asset('dash/vendor/counter-up/jquery.counterup.min.js')}}">
    </script>
    <script src="{{asset('dash/vendor/circle-progress/circle-progress.min.js')}}"></script>
    <script src="{{asset('dash/vendor/perfect-scrollbar/perfect-scrollbar.js')}}"></script>
    <script src="{{asset('dash/vendor/chartjs/Chart.bundle.min.js')}}"></script>
    <script src="{{asset('dash/vendor/select2/select2.min.js')}}">
    </script>
    @stack('scripts')

    <!-- Main JS-/->
    {{-- <script src="{{asset('dash/js/main.js')}}"></script> --}}