<script src="{{asset('home/js/jquery-1.12.0.min.js')}}"></script>
<!-- Bootstrap JS -->
<script src="{{asset('home/js/bootstrap.min.js')}}"></script>
<!-- Animate JS -->
<script src="{{asset('home/vendors/animate/wow.min.js')}}"></script>
<!-- Camera Slider -->
<script src="{{asset('home/vendors/camera-slider/jquery.easing.1.3.js')}}"></script>
<script src="{{asset('home/vendors/camera-slider/camera.min.js')}}"></script>
<!-- Isotope JS -->
<script src="{{asset('home/vendors/isotope/imagesloaded.pkgd.min.js')}}"></script>
<script src="{{asset('home/vendors/isotope/isotope.pkgd.min.js')}}"></script>
<!-- Progress JS -->
<script src="{{asset('home/vendors/Counter-Up/jquery.counterup.min.js')}}"></script>
<script src="{{asset('home/vendors/Counter-Up/waypoints.min.js')}}"></script>
<!-- Owlcarousel JS -->
<script src="{{asset('home/vendors/owl_carousel/owl.carousel.min.js')}}"></script>
<!-- Stellar JS -->
<script src="{{asset('home/vendors/stellar/jquery.stellar.js')}}"></script>
<!-- lightbox JS -->
<script src="{{asset('home/vendors/lightbox2/js/lightbox.js')}}"></script>
<!-- Theme JS -->
<script src="{{asset('home/js/theme.js')}}"></script>

{{ Html::script('dash/js/parsley.min.js') }}

@stack('scripts')
<script>
    var today = new Date();
var time = today.getHours() + ":" + today.getMinutes() +":"+ today.getSeconds();
</script>