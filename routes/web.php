<?php

//all pages routes
Route::get('/', 'PagesController@index')->name('pages.index');
Route::get('/galleries', 'PagesController@galleries')->name('pages.galleries');
Route::get('/contact', 'PagesController@contact')->name('pages.contact');
Route::get('/about', 'PagesController@about')->name('pages.about');
Route::get('/publications', 'PagesController@publications')->name('pages.publications');
Route::get('/events', 'PagesController@event')->name('pages.events');
Route::get('/event-details/{event}', 'PagesController@eventDetails')->name('pages.event-details');
Route::get('/events/{event}/register', 'PagesController@event_register_create')->name('pages.event_register.create');
Route::post('/events/register', 'PagesController@event_register_store')->name('pages.event_register.store');
Route::get('/training_mentorship', 'PagesController@training_mentorship')->name('pages.training_mentorship');
Route::get('/training_mentorship_single/{programme}', 'PagesController@training_mentorship_single')->name('pages.training_mentorship_single');
Route::get('/sponsors', 'PagesController@sponsors')->name('pages.sponsors');
Route::get('/search', 'PagesController@search')->name('pages.search');

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');


// starting of dashboard routes
Route::prefix('dashboard')->group(function () {

    //allow this page to be accessed by admin and auth users only
    Route::resource('/registration_code', 'RegistrationCodeController')->middleware('auth','admin');

    //administrator routes only
    Route::middleware(['web','admin'])->group(function () {
        Route::get('/', 'DashboardController@index')->name('dashboard.index');
        Route::resource('/event', 'EventController');
        Route::resource('/gallery', 'GalleryController');
        Route::resource('/title', 'TitleController');
        Route::resource('/assoc_school', 'AssocSchoolController');
        Route::resource('/sponsor', 'SponsorController');
        Route::resource('/publication', 'PublicationController');
        Route::resource('/publicationCategory', 'PublicationCategoryController');
        Route::resource('/slider', 'SliderController');
        Route::resource('/training_mentorship', 'TrainingMentorshipController');
        Route::resource('/constitution', 'ConstitutionController');
        Route::resource('/mission', 'MissionController');
        Route::resource('/contact', 'ContactController');
        Route::resource('/newsponsor', 'NewSponsorController')->except('create');
        Route::resource('/abstractresearch', 'AbstractResearchController');
        Route::resource('/account', 'AccountController');


    });
});

// end of dasboard routes