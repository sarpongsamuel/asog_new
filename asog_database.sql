-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 07, 2019 at 11:41 PM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `asog`
--
CREATE DATABASE IF NOT EXISTS `asog` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `asog`;

-- --------------------------------------------------------

--
-- Table structure for table `abstract_researches`
--
-- Error reading structure for table asog.abstract_researches: #1932 - Table 'asog.abstract_researches' doesn't exist in engine
-- Error reading data for table asog.abstract_researches: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `asog`.`abstract_researches`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `assoc_schools`
--
-- Error reading structure for table asog.assoc_schools: #1932 - Table 'asog.assoc_schools' doesn't exist in engine
-- Error reading data for table asog.assoc_schools: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `asog`.`assoc_schools`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `constitutions`
--
-- Error reading structure for table asog.constitutions: #1932 - Table 'asog.constitutions' doesn't exist in engine
-- Error reading data for table asog.constitutions: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `asog`.`constitutions`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--
-- Error reading structure for table asog.contacts: #1932 - Table 'asog.contacts' doesn't exist in engine
-- Error reading data for table asog.contacts: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `asog`.`contacts`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `events`
--
-- Error reading structure for table asog.events: #1932 - Table 'asog.events' doesn't exist in engine
-- Error reading data for table asog.events: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `asog`.`events`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `event_registers`
--
-- Error reading structure for table asog.event_registers: #1932 - Table 'asog.event_registers' doesn't exist in engine
-- Error reading data for table asog.event_registers: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `asog`.`event_registers`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `galleries`
--
-- Error reading structure for table asog.galleries: #1932 - Table 'asog.galleries' doesn't exist in engine
-- Error reading data for table asog.galleries: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `asog`.`galleries`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--
-- Error reading structure for table asog.migrations: #1932 - Table 'asog.migrations' doesn't exist in engine
-- Error reading data for table asog.migrations: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `asog`.`migrations`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `missions`
--
-- Error reading structure for table asog.missions: #1932 - Table 'asog.missions' doesn't exist in engine
-- Error reading data for table asog.missions: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `asog`.`missions`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `new_sponsors`
--
-- Error reading structure for table asog.new_sponsors: #1932 - Table 'asog.new_sponsors' doesn't exist in engine
-- Error reading data for table asog.new_sponsors: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `asog`.`new_sponsors`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--
-- Error reading structure for table asog.password_resets: #1932 - Table 'asog.password_resets' doesn't exist in engine
-- Error reading data for table asog.password_resets: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `asog`.`password_resets`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `publications`
--
-- Error reading structure for table asog.publications: #1932 - Table 'asog.publications' doesn't exist in engine
-- Error reading data for table asog.publications: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `asog`.`publications`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `publication_categories`
--
-- Error reading structure for table asog.publication_categories: #1932 - Table 'asog.publication_categories' doesn't exist in engine
-- Error reading data for table asog.publication_categories: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `asog`.`publication_categories`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `registration_codes`
--
-- Error reading structure for table asog.registration_codes: #1932 - Table 'asog.registration_codes' doesn't exist in engine
-- Error reading data for table asog.registration_codes: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `asog`.`registration_codes`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--
-- Error reading structure for table asog.sliders: #1932 - Table 'asog.sliders' doesn't exist in engine
-- Error reading data for table asog.sliders: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `asog`.`sliders`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `sponsors`
--
-- Error reading structure for table asog.sponsors: #1932 - Table 'asog.sponsors' doesn't exist in engine
-- Error reading data for table asog.sponsors: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `asog`.`sponsors`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `titles`
--
-- Error reading structure for table asog.titles: #1932 - Table 'asog.titles' doesn't exist in engine
-- Error reading data for table asog.titles: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `asog`.`titles`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `training_mentorships`
--
-- Error reading structure for table asog.training_mentorships: #1932 - Table 'asog.training_mentorships' doesn't exist in engine
-- Error reading data for table asog.training_mentorships: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `asog`.`training_mentorships`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `users`
--
-- Error reading structure for table asog.users: #1932 - Table 'asog.users' doesn't exist in engine
-- Error reading data for table asog.users: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `asog`.`users`' at line 1
--
-- Database: `boutique1`
--
CREATE DATABASE IF NOT EXISTS `boutique1` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `boutique1`;

-- --------------------------------------------------------

--
-- Table structure for table `brand`
--
-- Error reading structure for table boutique1.brand: #1932 - Table 'boutique1.brand' doesn't exist in engine
-- Error reading data for table boutique1.brand: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `boutique1`.`brand`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--
-- Error reading structure for table boutique1.cart: #1932 - Table 'boutique1.cart' doesn't exist in engine
-- Error reading data for table boutique1.cart: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `boutique1`.`cart`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--
-- Error reading structure for table boutique1.categories: #1932 - Table 'boutique1.categories' doesn't exist in engine
-- Error reading data for table boutique1.categories: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `boutique1`.`categories`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `products`
--
-- Error reading structure for table boutique1.products: #1932 - Table 'boutique1.products' doesn't exist in engine
-- Error reading data for table boutique1.products: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `boutique1`.`products`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `users`
--
-- Error reading structure for table boutique1.users: #1932 - Table 'boutique1.users' doesn't exist in engine
-- Error reading data for table boutique1.users: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `boutique1`.`users`' at line 1
--
-- Database: `church_register`
--
CREATE DATABASE IF NOT EXISTS `church_register` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `church_register`;
--
-- Database: `conference`
--
CREATE DATABASE IF NOT EXISTS `conference` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `conference`;

-- --------------------------------------------------------

--
-- Table structure for table `branches`
--
-- Error reading structure for table conference.branches: #1932 - Table 'conference.branches' doesn't exist in engine
-- Error reading data for table conference.branches: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `conference`.`branches`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `members`
--
-- Error reading structure for table conference.members: #1932 - Table 'conference.members' doesn't exist in engine
-- Error reading data for table conference.members: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `conference`.`members`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--
-- Error reading structure for table conference.migrations: #1932 - Table 'conference.migrations' doesn't exist in engine
-- Error reading data for table conference.migrations: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `conference`.`migrations`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--
-- Error reading structure for table conference.password_resets: #1932 - Table 'conference.password_resets' doesn't exist in engine
-- Error reading data for table conference.password_resets: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `conference`.`password_resets`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `sub__branches`
--
-- Error reading structure for table conference.sub__branches: #1932 - Table 'conference.sub__branches' doesn't exist in engine
-- Error reading data for table conference.sub__branches: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `conference`.`sub__branches`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `users`
--
-- Error reading structure for table conference.users: #1932 - Table 'conference.users' doesn't exist in engine
-- Error reading data for table conference.users: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `conference`.`users`' at line 1
--
-- Database: `conference19`
--
CREATE DATABASE IF NOT EXISTS `conference19` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `conference19`;

-- --------------------------------------------------------

--
-- Table structure for table `branches`
--
-- Error reading structure for table conference19.branches: #1932 - Table 'conference19.branches' doesn't exist in engine
-- Error reading data for table conference19.branches: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `conference19`.`branches`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `members`
--
-- Error reading structure for table conference19.members: #1932 - Table 'conference19.members' doesn't exist in engine
-- Error reading data for table conference19.members: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `conference19`.`members`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--
-- Error reading structure for table conference19.migrations: #1932 - Table 'conference19.migrations' doesn't exist in engine
-- Error reading data for table conference19.migrations: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `conference19`.`migrations`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--
-- Error reading structure for table conference19.oauth_access_tokens: #1932 - Table 'conference19.oauth_access_tokens' doesn't exist in engine
-- Error reading data for table conference19.oauth_access_tokens: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `conference19`.`oauth_access_tokens`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--
-- Error reading structure for table conference19.oauth_auth_codes: #1932 - Table 'conference19.oauth_auth_codes' doesn't exist in engine
-- Error reading data for table conference19.oauth_auth_codes: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `conference19`.`oauth_auth_codes`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--
-- Error reading structure for table conference19.oauth_clients: #1932 - Table 'conference19.oauth_clients' doesn't exist in engine
-- Error reading data for table conference19.oauth_clients: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `conference19`.`oauth_clients`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--
-- Error reading structure for table conference19.oauth_personal_access_clients: #1932 - Table 'conference19.oauth_personal_access_clients' doesn't exist in engine
-- Error reading data for table conference19.oauth_personal_access_clients: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `conference19`.`oauth_personal_access_clients`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--
-- Error reading structure for table conference19.oauth_refresh_tokens: #1932 - Table 'conference19.oauth_refresh_tokens' doesn't exist in engine
-- Error reading data for table conference19.oauth_refresh_tokens: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `conference19`.`oauth_refresh_tokens`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--
-- Error reading structure for table conference19.password_resets: #1932 - Table 'conference19.password_resets' doesn't exist in engine
-- Error reading data for table conference19.password_resets: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `conference19`.`password_resets`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `schools`
--
-- Error reading structure for table conference19.schools: #1932 - Table 'conference19.schools' doesn't exist in engine
-- Error reading data for table conference19.schools: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `conference19`.`schools`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `sub_branches`
--
-- Error reading structure for table conference19.sub_branches: #1932 - Table 'conference19.sub_branches' doesn't exist in engine
-- Error reading data for table conference19.sub_branches: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `conference19`.`sub_branches`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `users`
--
-- Error reading structure for table conference19.users: #1932 - Table 'conference19.users' doesn't exist in engine
-- Error reading data for table conference19.users: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `conference19`.`users`' at line 1
--
-- Database: `document`
--
CREATE DATABASE IF NOT EXISTS `document` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `document`;

-- --------------------------------------------------------

--
-- Table structure for table `divisions`
--
-- Error reading structure for table document.divisions: #1932 - Table 'document.divisions' doesn't exist in engine
-- Error reading data for table document.divisions: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `document`.`divisions`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `files`
--
-- Error reading structure for table document.files: #1932 - Table 'document.files' doesn't exist in engine
-- Error reading data for table document.files: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `document`.`files`' at line 1
--
-- Database: `eshop`
--
CREATE DATABASE IF NOT EXISTS `eshop` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `eshop`;

-- --------------------------------------------------------

--
-- Table structure for table `account`
--
-- Error reading structure for table eshop.account: #1932 - Table 'eshop.account' doesn't exist in engine
-- Error reading data for table eshop.account: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `eshop`.`account`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--
-- Error reading structure for table eshop.admin: #1932 - Table 'eshop.admin' doesn't exist in engine
-- Error reading data for table eshop.admin: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `eshop`.`admin`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--
-- Error reading structure for table eshop.brands: #1932 - Table 'eshop.brands' doesn't exist in engine
-- Error reading data for table eshop.brands: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `eshop`.`brands`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--
-- Error reading structure for table eshop.cart: #1932 - Table 'eshop.cart' doesn't exist in engine
-- Error reading data for table eshop.cart: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `eshop`.`cart`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `category`
--
-- Error reading structure for table eshop.category: #1932 - Table 'eshop.category' doesn't exist in engine
-- Error reading data for table eshop.category: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `eshop`.`category`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--
-- Error reading structure for table eshop.orders: #1932 - Table 'eshop.orders' doesn't exist in engine
-- Error reading data for table eshop.orders: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `eshop`.`orders`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `products`
--
-- Error reading structure for table eshop.products: #1932 - Table 'eshop.products' doesn't exist in engine
-- Error reading data for table eshop.products: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `eshop`.`products`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--
-- Error reading structure for table eshop.reviews: #1932 - Table 'eshop.reviews' doesn't exist in engine
-- Error reading data for table eshop.reviews: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `eshop`.`reviews`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--
-- Error reading structure for table eshop.roles: #1932 - Table 'eshop.roles' doesn't exist in engine
-- Error reading data for table eshop.roles: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `eshop`.`roles`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `users`
--
-- Error reading structure for table eshop.users: #1932 - Table 'eshop.users' doesn't exist in engine
-- Error reading data for table eshop.users: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `eshop`.`users`' at line 1
--
-- Database: `marciana`
--
CREATE DATABASE IF NOT EXISTS `marciana` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `marciana`;

-- --------------------------------------------------------

--
-- Table structure for table `checkout`
--
-- Error reading structure for table marciana.checkout: #1932 - Table 'marciana.checkout' doesn't exist in engine
-- Error reading data for table marciana.checkout: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `marciana`.`checkout`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `emails`
--
-- Error reading structure for table marciana.emails: #1932 - Table 'marciana.emails' doesn't exist in engine
-- Error reading data for table marciana.emails: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `marciana`.`emails`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--
-- Error reading structure for table marciana.gallery: #1932 - Table 'marciana.gallery' doesn't exist in engine
-- Error reading data for table marciana.gallery: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `marciana`.`gallery`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `gym`
--
-- Error reading structure for table marciana.gym: #1932 - Table 'marciana.gym' doesn't exist in engine
-- Error reading data for table marciana.gym: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `marciana`.`gym`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `gym_image`
--
-- Error reading structure for table marciana.gym_image: #1932 - Table 'marciana.gym_image' doesn't exist in engine
-- Error reading data for table marciana.gym_image: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `marciana`.`gym_image`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `images`
--
-- Error reading structure for table marciana.images: #1932 - Table 'marciana.images' doesn't exist in engine
-- Error reading data for table marciana.images: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `marciana`.`images`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `meeting`
--
-- Error reading structure for table marciana.meeting: #1932 - Table 'marciana.meeting' doesn't exist in engine
-- Error reading data for table marciana.meeting: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `marciana`.`meeting`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `meeting_image`
--
-- Error reading structure for table marciana.meeting_image: #1932 - Table 'marciana.meeting_image' doesn't exist in engine
-- Error reading data for table marciana.meeting_image: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `marciana`.`meeting_image`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--
-- Error reading structure for table marciana.payment: #1932 - Table 'marciana.payment' doesn't exist in engine
-- Error reading data for table marciana.payment: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `marciana`.`payment`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--
-- Error reading structure for table marciana.reviews: #1932 - Table 'marciana.reviews' doesn't exist in engine
-- Error reading data for table marciana.reviews: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `marciana`.`reviews`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `rooms`
--
-- Error reading structure for table marciana.rooms: #1932 - Table 'marciana.rooms' doesn't exist in engine
-- Error reading data for table marciana.rooms: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `marciana`.`rooms`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `spa`
--
-- Error reading structure for table marciana.spa: #1932 - Table 'marciana.spa' doesn't exist in engine
-- Error reading data for table marciana.spa: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `marciana`.`spa`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `spa_images`
--
-- Error reading structure for table marciana.spa_images: #1932 - Table 'marciana.spa_images' doesn't exist in engine
-- Error reading data for table marciana.spa_images: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `marciana`.`spa_images`' at line 1
--
-- Database: `nupsgknust_db`
--
CREATE DATABASE IF NOT EXISTS `nupsgknust_db` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `nupsgknust_db`;

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--
-- Error reading structure for table nupsgknust_db.blogs: #1932 - Table 'nupsgknust_db.blogs' doesn't exist in engine
-- Error reading data for table nupsgknust_db.blogs: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `nupsgknust_db`.`blogs`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `events`
--
-- Error reading structure for table nupsgknust_db.events: #1932 - Table 'nupsgknust_db.events' doesn't exist in engine
-- Error reading data for table nupsgknust_db.events: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `nupsgknust_db`.`events`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `executives`
--
-- Error reading structure for table nupsgknust_db.executives: #1932 - Table 'nupsgknust_db.executives' doesn't exist in engine
-- Error reading data for table nupsgknust_db.executives: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `nupsgknust_db`.`executives`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--
-- Error reading structure for table nupsgknust_db.migrations: #1932 - Table 'nupsgknust_db.migrations' doesn't exist in engine
-- Error reading data for table nupsgknust_db.migrations: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `nupsgknust_db`.`migrations`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--
-- Error reading structure for table nupsgknust_db.password_resets: #1932 - Table 'nupsgknust_db.password_resets' doesn't exist in engine
-- Error reading data for table nupsgknust_db.password_resets: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `nupsgknust_db`.`password_resets`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `patrons`
--
-- Error reading structure for table nupsgknust_db.patrons: #1932 - Table 'nupsgknust_db.patrons' doesn't exist in engine
-- Error reading data for table nupsgknust_db.patrons: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `nupsgknust_db`.`patrons`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `personalities`
--
-- Error reading structure for table nupsgknust_db.personalities: #1932 - Table 'nupsgknust_db.personalities' doesn't exist in engine
-- Error reading data for table nupsgknust_db.personalities: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `nupsgknust_db`.`personalities`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `sermons`
--
-- Error reading structure for table nupsgknust_db.sermons: #1932 - Table 'nupsgknust_db.sermons' doesn't exist in engine
-- Error reading data for table nupsgknust_db.sermons: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `nupsgknust_db`.`sermons`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `users`
--
-- Error reading structure for table nupsgknust_db.users: #1932 - Table 'nupsgknust_db.users' doesn't exist in engine
-- Error reading data for table nupsgknust_db.users: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `nupsgknust_db`.`users`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `wings`
--
-- Error reading structure for table nupsgknust_db.wings: #1932 - Table 'nupsgknust_db.wings' doesn't exist in engine
-- Error reading data for table nupsgknust_db.wings: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `nupsgknust_db`.`wings`' at line 1
--
-- Database: `nupsgmember_db`
--
CREATE DATABASE IF NOT EXISTS `nupsgmember_db` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `nupsgmember_db`;

-- --------------------------------------------------------

--
-- Table structure for table `colleges`
--
-- Error reading structure for table nupsgmember_db.colleges: #1932 - Table 'nupsgmember_db.colleges' doesn't exist in engine
-- Error reading data for table nupsgmember_db.colleges: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `nupsgmember_db`.`colleges`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `halls`
--
-- Error reading structure for table nupsgmember_db.halls: #1932 - Table 'nupsgmember_db.halls' doesn't exist in engine
-- Error reading data for table nupsgmember_db.halls: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `nupsgmember_db`.`halls`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `levels`
--
-- Error reading structure for table nupsgmember_db.levels: #1932 - Table 'nupsgmember_db.levels' doesn't exist in engine
-- Error reading data for table nupsgmember_db.levels: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `nupsgmember_db`.`levels`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `members`
--
-- Error reading structure for table nupsgmember_db.members: #1932 - Table 'nupsgmember_db.members' doesn't exist in engine
-- Error reading data for table nupsgmember_db.members: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `nupsgmember_db`.`members`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `member_wing`
--
-- Error reading structure for table nupsgmember_db.member_wing: #1932 - Table 'nupsgmember_db.member_wing' doesn't exist in engine
-- Error reading data for table nupsgmember_db.member_wing: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `nupsgmember_db`.`member_wing`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--
-- Error reading structure for table nupsgmember_db.migrations: #1932 - Table 'nupsgmember_db.migrations' doesn't exist in engine
-- Error reading data for table nupsgmember_db.migrations: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `nupsgmember_db`.`migrations`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--
-- Error reading structure for table nupsgmember_db.password_resets: #1932 - Table 'nupsgmember_db.password_resets' doesn't exist in engine
-- Error reading data for table nupsgmember_db.password_resets: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `nupsgmember_db`.`password_resets`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `programmes`
--
-- Error reading structure for table nupsgmember_db.programmes: #1932 - Table 'nupsgmember_db.programmes' doesn't exist in engine
-- Error reading data for table nupsgmember_db.programmes: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `nupsgmember_db`.`programmes`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `regions`
--
-- Error reading structure for table nupsgmember_db.regions: #1932 - Table 'nupsgmember_db.regions' doesn't exist in engine
-- Error reading data for table nupsgmember_db.regions: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `nupsgmember_db`.`regions`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `residences`
--
-- Error reading structure for table nupsgmember_db.residences: #1932 - Table 'nupsgmember_db.residences' doesn't exist in engine
-- Error reading data for table nupsgmember_db.residences: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `nupsgmember_db`.`residences`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `users`
--
-- Error reading structure for table nupsgmember_db.users: #1932 - Table 'nupsgmember_db.users' doesn't exist in engine
-- Error reading data for table nupsgmember_db.users: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `nupsgmember_db`.`users`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `wings`
--
-- Error reading structure for table nupsgmember_db.wings: #1932 - Table 'nupsgmember_db.wings' doesn't exist in engine
-- Error reading data for table nupsgmember_db.wings: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `nupsgmember_db`.`wings`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `zones`
--
-- Error reading structure for table nupsgmember_db.zones: #1932 - Table 'nupsgmember_db.zones' doesn't exist in engine
-- Error reading data for table nupsgmember_db.zones: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `nupsgmember_db`.`zones`' at line 1
--
-- Database: `phpmyadmin`
--
CREATE DATABASE IF NOT EXISTS `phpmyadmin` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;
USE `phpmyadmin`;

-- --------------------------------------------------------

--
-- Table structure for table `pma__bookmark`
--

CREATE TABLE `pma__bookmark` (
  `id` int(10) UNSIGNED NOT NULL,
  `dbase` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `user` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `label` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `query` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Bookmarks';

-- --------------------------------------------------------

--
-- Table structure for table `pma__central_columns`
--

CREATE TABLE `pma__central_columns` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `col_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `col_type` varchar(64) COLLATE utf8_bin NOT NULL,
  `col_length` text COLLATE utf8_bin DEFAULT NULL,
  `col_collation` varchar(64) COLLATE utf8_bin NOT NULL,
  `col_isNull` tinyint(1) NOT NULL,
  `col_extra` varchar(255) COLLATE utf8_bin DEFAULT '',
  `col_default` text COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Central list of columns';

-- --------------------------------------------------------

--
-- Table structure for table `pma__column_info`
--

CREATE TABLE `pma__column_info` (
  `id` int(5) UNSIGNED NOT NULL,
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `column_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `comment` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `mimetype` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `transformation` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `transformation_options` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `input_transformation` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `input_transformation_options` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Column information for phpMyAdmin';

-- --------------------------------------------------------

--
-- Table structure for table `pma__designer_settings`
--

CREATE TABLE `pma__designer_settings` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `settings_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Settings related to Designer';

-- --------------------------------------------------------

--
-- Table structure for table `pma__export_templates`
--

CREATE TABLE `pma__export_templates` (
  `id` int(5) UNSIGNED NOT NULL,
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `export_type` varchar(10) COLLATE utf8_bin NOT NULL,
  `template_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `template_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Saved export templates';

-- --------------------------------------------------------

--
-- Table structure for table `pma__favorite`
--

CREATE TABLE `pma__favorite` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `tables` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Favorite tables';

-- --------------------------------------------------------

--
-- Table structure for table `pma__history`
--

CREATE TABLE `pma__history` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `username` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `db` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `timevalue` timestamp NOT NULL DEFAULT current_timestamp(),
  `sqlquery` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='SQL history for phpMyAdmin';

-- --------------------------------------------------------

--
-- Table structure for table `pma__navigationhiding`
--

CREATE TABLE `pma__navigationhiding` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `item_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `item_type` varchar(64) COLLATE utf8_bin NOT NULL,
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Hidden items of navigation tree';

-- --------------------------------------------------------

--
-- Table structure for table `pma__pdf_pages`
--

CREATE TABLE `pma__pdf_pages` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `page_nr` int(10) UNSIGNED NOT NULL,
  `page_descr` varchar(50) CHARACTER SET utf8 NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='PDF relation pages for phpMyAdmin';

-- --------------------------------------------------------

--
-- Table structure for table `pma__recent`
--

CREATE TABLE `pma__recent` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `tables` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Recently accessed tables';

-- --------------------------------------------------------

--
-- Table structure for table `pma__relation`
--

CREATE TABLE `pma__relation` (
  `master_db` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `master_table` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `master_field` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `foreign_db` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `foreign_table` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `foreign_field` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Relation table';

-- --------------------------------------------------------

--
-- Table structure for table `pma__savedsearches`
--

CREATE TABLE `pma__savedsearches` (
  `id` int(5) UNSIGNED NOT NULL,
  `username` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `search_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `search_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Saved searches';

-- --------------------------------------------------------

--
-- Table structure for table `pma__table_coords`
--

CREATE TABLE `pma__table_coords` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `pdf_page_number` int(11) NOT NULL DEFAULT 0,
  `x` float UNSIGNED NOT NULL DEFAULT 0,
  `y` float UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table coordinates for phpMyAdmin PDF output';

-- --------------------------------------------------------

--
-- Table structure for table `pma__table_info`
--

CREATE TABLE `pma__table_info` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `display_field` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table information for phpMyAdmin';

-- --------------------------------------------------------

--
-- Table structure for table `pma__table_uiprefs`
--

CREATE TABLE `pma__table_uiprefs` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `prefs` text COLLATE utf8_bin NOT NULL,
  `last_update` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Tables'' UI preferences';

-- --------------------------------------------------------

--
-- Table structure for table `pma__tracking`
--

CREATE TABLE `pma__tracking` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `version` int(10) UNSIGNED NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL,
  `schema_snapshot` text COLLATE utf8_bin NOT NULL,
  `schema_sql` text COLLATE utf8_bin DEFAULT NULL,
  `data_sql` longtext COLLATE utf8_bin DEFAULT NULL,
  `tracking` set('UPDATE','REPLACE','INSERT','DELETE','TRUNCATE','CREATE DATABASE','ALTER DATABASE','DROP DATABASE','CREATE TABLE','ALTER TABLE','RENAME TABLE','DROP TABLE','CREATE INDEX','DROP INDEX','CREATE VIEW','ALTER VIEW','DROP VIEW') COLLATE utf8_bin DEFAULT NULL,
  `tracking_active` int(1) UNSIGNED NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Database changes tracking for phpMyAdmin';

-- --------------------------------------------------------

--
-- Table structure for table `pma__userconfig`
--

CREATE TABLE `pma__userconfig` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `timevalue` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `config_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='User preferences storage for phpMyAdmin';

--
-- Dumping data for table `pma__userconfig`
--

INSERT INTO `pma__userconfig` (`username`, `timevalue`, `config_data`) VALUES
('root', '2019-08-07 21:41:33', '{\"Console\\/Mode\":\"collapse\"}');

-- --------------------------------------------------------

--
-- Table structure for table `pma__usergroups`
--

CREATE TABLE `pma__usergroups` (
  `usergroup` varchar(64) COLLATE utf8_bin NOT NULL,
  `tab` varchar(64) COLLATE utf8_bin NOT NULL,
  `allowed` enum('Y','N') COLLATE utf8_bin NOT NULL DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='User groups with configured menu items';

-- --------------------------------------------------------

--
-- Table structure for table `pma__users`
--

CREATE TABLE `pma__users` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `usergroup` varchar(64) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Users and their assignments to user groups';

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pma__bookmark`
--
ALTER TABLE `pma__bookmark`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pma__central_columns`
--
ALTER TABLE `pma__central_columns`
  ADD PRIMARY KEY (`db_name`,`col_name`);

--
-- Indexes for table `pma__column_info`
--
ALTER TABLE `pma__column_info`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `db_name` (`db_name`,`table_name`,`column_name`);

--
-- Indexes for table `pma__designer_settings`
--
ALTER TABLE `pma__designer_settings`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `pma__export_templates`
--
ALTER TABLE `pma__export_templates`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `u_user_type_template` (`username`,`export_type`,`template_name`);

--
-- Indexes for table `pma__favorite`
--
ALTER TABLE `pma__favorite`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `pma__history`
--
ALTER TABLE `pma__history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `username` (`username`,`db`,`table`,`timevalue`);

--
-- Indexes for table `pma__navigationhiding`
--
ALTER TABLE `pma__navigationhiding`
  ADD PRIMARY KEY (`username`,`item_name`,`item_type`,`db_name`,`table_name`);

--
-- Indexes for table `pma__pdf_pages`
--
ALTER TABLE `pma__pdf_pages`
  ADD PRIMARY KEY (`page_nr`),
  ADD KEY `db_name` (`db_name`);

--
-- Indexes for table `pma__recent`
--
ALTER TABLE `pma__recent`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `pma__relation`
--
ALTER TABLE `pma__relation`
  ADD PRIMARY KEY (`master_db`,`master_table`,`master_field`),
  ADD KEY `foreign_field` (`foreign_db`,`foreign_table`);

--
-- Indexes for table `pma__savedsearches`
--
ALTER TABLE `pma__savedsearches`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `u_savedsearches_username_dbname` (`username`,`db_name`,`search_name`);

--
-- Indexes for table `pma__table_coords`
--
ALTER TABLE `pma__table_coords`
  ADD PRIMARY KEY (`db_name`,`table_name`,`pdf_page_number`);

--
-- Indexes for table `pma__table_info`
--
ALTER TABLE `pma__table_info`
  ADD PRIMARY KEY (`db_name`,`table_name`);

--
-- Indexes for table `pma__table_uiprefs`
--
ALTER TABLE `pma__table_uiprefs`
  ADD PRIMARY KEY (`username`,`db_name`,`table_name`);

--
-- Indexes for table `pma__tracking`
--
ALTER TABLE `pma__tracking`
  ADD PRIMARY KEY (`db_name`,`table_name`,`version`);

--
-- Indexes for table `pma__userconfig`
--
ALTER TABLE `pma__userconfig`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `pma__usergroups`
--
ALTER TABLE `pma__usergroups`
  ADD PRIMARY KEY (`usergroup`,`tab`,`allowed`);

--
-- Indexes for table `pma__users`
--
ALTER TABLE `pma__users`
  ADD PRIMARY KEY (`username`,`usergroup`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pma__bookmark`
--
ALTER TABLE `pma__bookmark`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pma__column_info`
--
ALTER TABLE `pma__column_info`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pma__export_templates`
--
ALTER TABLE `pma__export_templates`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pma__history`
--
ALTER TABLE `pma__history`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pma__pdf_pages`
--
ALTER TABLE `pma__pdf_pages`
  MODIFY `page_nr` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pma__savedsearches`
--
ALTER TABLE `pma__savedsearches`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- Database: `pmanager`
--
CREATE DATABASE IF NOT EXISTS `pmanager` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `pmanager`;

-- --------------------------------------------------------

--
-- Table structure for table `chats`
--
-- Error reading structure for table pmanager.chats: #1932 - Table 'pmanager.chats' doesn't exist in engine
-- Error reading data for table pmanager.chats: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `pmanager`.`chats`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `company`
--
-- Error reading structure for table pmanager.company: #1932 - Table 'pmanager.company' doesn't exist in engine
-- Error reading data for table pmanager.company: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `pmanager`.`company`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `project`
--
-- Error reading structure for table pmanager.project: #1932 - Table 'pmanager.project' doesn't exist in engine
-- Error reading data for table pmanager.project: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `pmanager`.`project`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `task`
--
-- Error reading structure for table pmanager.task: #1932 - Table 'pmanager.task' doesn't exist in engine
-- Error reading data for table pmanager.task: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `pmanager`.`task`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `users`
--
-- Error reading structure for table pmanager.users: #1932 - Table 'pmanager.users' doesn't exist in engine
-- Error reading data for table pmanager.users: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `pmanager`.`users`' at line 1
--
-- Database: `test`
--
CREATE DATABASE IF NOT EXISTS `test` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `test`;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
