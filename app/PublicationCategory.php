<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PublicationCategory extends Model
{
    public function publications(){
        return $this->hasMany('App\Publication');
    }
}
