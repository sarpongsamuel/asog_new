<?php

namespace App\Http\Controllers;

use App\RegistrationCode;
use Illuminate\Http\Request;
use Session;

class RegistrationCodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    // public function __construct()
    // {
    //     $this->middleware(['admin']);
        
    // }
    
    public function index()
    {
        $codes = RegistrationCode::all();

        return view('registration_code.index')
            ->withCodes($codes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        RegistrationCode::truncate();

        for ($index = 0; $index < 400; $index++) {
            $code = RegistrationCode::generateRegistrationCode(7);

            $registrationCode = new RegistrationCode;

            $registrationCode->code = $code;

            $registrationCode->save();
        }

        Session::flash('success', 'Codes generated successfully');

        return redirect()->route('registration_code.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\RegistrationCode  $registrationCode
     * @return \Illuminate\Http\Response
     */
    public function show(RegistrationCode $registrationCode)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RegistrationCode  $registrationCode
     * @return \Illuminate\Http\Response
     */
    public function edit(RegistrationCode $registrationCode)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RegistrationCode  $registrationCode
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RegistrationCode $registrationCode)
    {
        $this->validate($request, array(
            'phone' => 'required|regex:/[0-9]/|unique:registration_codes,phone,' . $registrationCode->id . '|max:10',
        ),array(
            'phone.unique' => 'Phone number has already been assigned a registration code'
        ));

        $registrationCode->phone = $request->phone;

        $registrationCode->save();

        Session::flash('success', 'Code assigned successfully');

        return redirect()->route('registration_code.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RegistrationCode  $registrationCode
     * @return \Illuminate\Http\Response
     */
    public function destroy(RegistrationCode $registrationCode)
    {
        $registrationCode->delete();

        Session::flash('success', 'Code deleted successfully');

        return redirect()->route('registration_code.index');
    }
}
