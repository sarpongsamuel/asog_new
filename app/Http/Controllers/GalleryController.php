<?php

namespace App\Http\Controllers;

use App\Gallery;
use Illuminate\Http\Request;
use Image;
use Session;

class GalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    // public function __construct()
    // {
    //     $this->middleware(['admin']);
        
    // }
    
    public function index()
    {
        //fetch all gallery images
        $gallery = Gallery::all();
        return view('gallery.index')
            ->withImage($gallery);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('gallery.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request;
        $this->validate($request, array(
            'name' => 'required|string',
            // 'photo'=>'required|mimes:jpg,png,jpeg',
        ));

        if ($request->hasfile('photo')) {

            foreach ($request->file('photo') as $image) {

                //create a file name using time and append image ext
                $filename = time() . '.' . $image->getClientOriginalExtension();
                // set the location for the file
                $location = public_path('images/gallery/' . $filename);
                // resize maintaining aspect ratio and saving to location
                Image::make($image)->resize(927, 623, function ($constraint) {
                    $constraint->aspectRatio();
                })->fit(997, 623)->save($location);
                //set the value of wings in the db to file name

                //    $name=$image->getClientOriginalName();

                //    $image->move(public_path().'/gallery/', $name);
                $data[] = $filename;

            }
            //loop through data and insert image
            foreach ($data as $i) {

                $img = new Gallery;
                $img->name = $request->name;
                $img->image = $i;
                $img->save();
            }

        }

        Session::flash('success', 'image added successfully');
        return redirect()->route('gallery.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $image = Gallery::find($id);
        return view('gallery.edit')
            ->withGallery($image);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, array(
            'name' => 'required|string',
            'photo' => 'required|mimes:jpg,png,jpeg',
        ));

        $gallery = Gallery::find($id);
        $gallery->name = $request->name;
        $gallery->image = $request->photo;

        if ($request->hasFile('photo')) {
            // get the image
            $image = $request->file('photo');
            //create a file name using time and append image ext
            $filename = time() . '.' . $image->getClientOriginalExtension();
            // set the location for the file
            $location = public_path('images/gallery/' . $filename);
            // resize maintaining aspect ratio and saving to location
            Image::make($image)->resize(927, 623, function ($constraint) {
                $constraint->aspectRatio();
            })->fit(997, 623)->save($location);
            //set the value of wings in the db to file name
            $gallery->image = $filename;
        }

        $gallery->save();

        Session::flash('success', 'image updated successfully');
        return redirect()->route('gallery.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $image = Gallery::find($id);
        $image->delete();

        Session::flash('success', 'image deleted successfully');
        return redirect()->route('gallery.index');

    }
}
