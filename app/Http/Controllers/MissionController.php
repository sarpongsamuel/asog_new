<?php

namespace App\Http\Controllers;
use App\Mission;
use Session;
use Illuminate\Http\Request;

class MissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    // public function __construct()
    // {
    //     $this->middleware(['admin']);
        
    // }
    
    public function index()
    {
        $mission = Mission::all()->take(1);
        return view('mission.index')
                ->withMission($mission);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('mission.create');
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, array(
            'context'=>'required|string',
        ));

        $mission = new Mission;
        $mission->context = $request->context;
        $mission->save();
        
        Session::flash('success','added successfully');
        return redirect()->route('mission.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $mission = Mission::find($id);
        return view('mission.edit')->withMission($mission);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, array(
            'context'=>'required|string',
        ));

        $mission = Mission::find($id);
        $mission->context = $request->context;
        $mission->save();
        
        Session::flash('success','Updated successfully');
        return redirect()->route('mission.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // return $id;
        $mission = Mission::find($id);
        
        $mission->delete();
        
        Session::flash('success','Updated successfully');
        return redirect()->route('mission.index');
    }
}
