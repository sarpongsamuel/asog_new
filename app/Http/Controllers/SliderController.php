<?php

namespace App\Http\Controllers;

use App\Slider;
use Illuminate\Http\Request;
use Image;
use Session;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    // public function __construct()
    // {
    //     $this->middleware(['admin']);
        
    // }
    
    public function index()
    {
        $slides = Slider::all();
        return view('slider.index')
            ->withSliders($slides);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('slider.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request;
        $this->validate($request, array(
            'header' => '',
            'image' => 'required|mimes:png,jpg,jpeg',
            'context' => '',
        ));

        $slider = new Slider;
        $slider->header = strtoupper($request->header);
        $slider->image = $request->image;
        $slider->context = $request->context;

        // use image processor here
        if ($request->hasFile('image')) {
            // get the image
            $image = $request->file('image');
            //create a file name using time and append image ext
            $filename = time() . '.' . $image->getClientOriginalExtension();
            // set the location for the file
            $location = public_path('images/slide/' . $filename);
            // resize maintaining aspect ratio and saving to location
            Image::make($image)->save($location);
            //set the value of wings in the db to file name
            $slider->image = $filename;
        }

        $slider->save();

        Session::flash('success', $request->name . 'Added successfully');
        return redirect()->route('slider.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $slide = Slider::find($id);
        return view('slider.show')
            ->withSlider($slide);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $slide = Slider::find($id);
        return view('slider.edit')
            ->withSlider($slide);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // return $request;
        $this->validate($request, array(
            'header' => 'string',
            'image' => 'required|mimes:png,jpg,jpeg',
            'context' => 'string',
        ));

        $slider = Slider::find($id);
        $slider->header = strtoupper($request->header);
        $slider->image = $request->image;
        $slider->context = $request->context;

        // use image processor here
        if ($request->hasFile('image')) {
            // get the image
            $image = $request->file('image');
            //create a file name using time and append image ext
            $filename = time() . '.' . $image->getClientOriginalExtension();
            // set the location for the file
            $location = public_path('images/slide/' . $filename);
            // resize maintaining aspect ratio and saving to location
            Image::make($image)->save($location);
            //set the value of wings in the db to file name
            $slider->image = $filename;
        }

        $slider->save();

        Session::flash('success', $request->name . 'Updated successfully');
        return redirect()->route('slider.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $slide = Slider::find($id);
        $slide->delete();

        Session::flash('success', 'Deleted Successfully');
        return redirect()->route('slider.index');
    }
}
