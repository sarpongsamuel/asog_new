<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PublicationCategory;
use Session;
class PublicationCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function __construct()
    // {
    //     $this->middleware(['admin']);
        
    // }
    
    public function index()
    {
        $pub_category = PublicationCategory::paginate(10);
        return view('publicationCategory.index')
                ->withCategories($pub_category);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, array(
            'name' => 'required|string',
        ));

        $category = new PublicationCategory;
        $category->name = strtoupper($request->name);

        $category->save();
        Session::flash('success',$request->name." have been added successfully");
        return redirect()->route('publicationCategory.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = PublicationCategory::find($id);
        $category->delete();
        Session::flash('success','deleted successfully');
        return redirect()->route('publicationCategory.index');
    }
}
