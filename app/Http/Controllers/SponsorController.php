<?php

namespace App\Http\Controllers;

use App\Sponsor;
use Illuminate\Http\Request;
use Image;
use Purifier;
use Session;

class SponsorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function __construct()
    // {
    //     $this->middleware(['admin']);
        
    // }
    
    public function index()
    {
        $sponsors = Sponsor::all();
        return view('sponsor.index')
            ->withSponsors($sponsors);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('sponsor.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, array(
            'name' => 'required|string',
            'logo' => 'required|mimes:png,jpg,jpeg',
            'details' => 'required|string',
            'location' => 'required|string',
            'contact1' => 'required|max:10,min:10',
            'contact2' => 'max:10,min:10',

        ));

        $sponsor = new Sponsor;
        $sponsor->name = strtoupper($request->name);
        $sponsor->logo = $request->logo;
        $sponsor->details = Purifier::clean($request->details);
        $sponsor->location = $request->location;
        $sponsor->contact1 = $request->contact1;
        $sponsor->contact2 = $request->contact2;

        // use image processor here
        if ($request->hasFile('logo')) {
            // get the image
            $image = $request->file('logo');
            //create a file name using time and append image ext
            $filename = time() . '.' . $image->getClientOriginalExtension();
            // set the location for the file
            $location = public_path('images/sponsor/' . $filename);
            // resize maintaining aspect ratio and saving to location
            Image::make($image)->resize(420, 320, function ($constraint) {
                $constraint->aspectRatio();
            })->resizeCanvas(420, 320, 'top')->save($location);
            //set the value of wings in the db to file name
            $sponsor->logo = $filename;
        }

        $sponsor->save();

        Session::flash('success', $request->name . 'Added successfully');
        return redirect()->route('sponsor.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $sponsor = Sponsor::find($id);
        return view('sponsor.show')
            ->withSponsor($sponsor);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sponsor = Sponsor::find($id);
        return view('sponsor.edit')
            ->withSponsor($sponsor);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, array(
            'name' => 'required|string',
            'logo' => 'required|mimes:png,jpg,jpeg',
            'details' => 'required|string',
            'location' => 'required|string',
            'contact1' => 'required|max:10,min:10',
            'contact2' => 'max:10,min:10',

        ));

        $sponsor = Sponsor::find($id);
        $sponsor->name = strtoupper($request->name);
        $sponsor->logo = $request->logo;
        $sponsor->details = Purifier::clean($request->details);
        $sponsor->location = $request->location;
        $sponsor->contact1 = $request->contact1;
        $sponsor->contact2 = $request->contact2;

        // use image processor here
        if ($request->hasFile('logo')) {
            // get the image
            $image = $request->file('logo');
            //create a file name using time and append image ext
            $filename = time() . '.' . $image->getClientOriginalExtension();
            // set the location for the file
            $location = public_path('images/sponsor/' . $filename);
            // resize maintaining aspect ratio and saving to location
            Image::make($image)->resize(420, 320, function ($constraint) {
                $constraint->aspectRatio();
            })->resizeCanvas(420, 320, 'top')->save($location);
            //set the value of wings in the db to file name
            $sponsor->logo = $filename;
        }

        $sponsor->save();

        Session::flash('success', $request->name . 'Updated Successfully');
        return redirect()->route('sponsor.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sponsor = Sponsor::find($id);
        $sponsor->delete();

        Session::flash('success', ' Deleted successfully');
        return redirect()->route('sponsor.index');
    }

}
