<?php

namespace App\Http\Controllers;

use App\Constitution;
use App\Event;
use App\EventRegister;
use App\Gallery;
use App\Mission;
use App\Publication;
use App\PublicationCategory;
use App\RegistrationCode;
use App\Slider;
use App\Sponsor;
use App\Title;
use App\TrainingMentorship;
use App\AbstractResearch;
use Illuminate\Http\Request;
use Session;

class PagesController extends Controller
{
    public function index()
    {

        $slide = Slider::all();

        $sponsors = Sponsor::all();

        $homeEvent = Event::first();

        $mission = Mission::first();

        $programme = TrainingMentorship::first();

        
        
        return view('pages.index')
            ->withSponsors($sponsors)
            ->withSlider($slide)
            ->withEvent($homeEvent)
            ->withProgramme($programme)
            ->withMission($mission);
    }

    //-------------------------------------------------gallery image --------------------------------------------------------
    public function galleries()
    {

        $images = Gallery::paginate(18);

        // return $images;
        return view('pages.galleries')
            ->withImages($images);
    }

    // ------------------------------------------------contact page -------------------------------------------------
    public function contact()
    {
        return view('pages.contact');
    }

// ------------------------------------------------------------------about page----------------------------------------------
    public function about()
    {
        $mission = Mission::first();

        $constitution = Constitution::first();

        return view('pages.about')
            ->withMission($mission)
            ->withConstitution($constitution);
    }

//----------------------------------------------------------------publications -----------------------------------------------
    public function publications()
    {

        $categories = PublicationCategory::all();
        $abstract = AbstractResearch::where('publication_type','like','Abstract%')->get();
        $proposal = AbstractResearch::where('publication_type','like','Research Proposal%')->get();

        return view('pages.publications')
            ->withCategories($categories)
            ->withAbstracts($abstract)
            ->withProposals($proposal);
    }
    // -------------------------------------------------------events --------------------------------------------------------------
    public function event()
    {

        $events = Event::OrderBy('id', 'desc')->paginate(10);

        return view('pages.events')
            ->withEvents($events);
    }

    public function eventDetails($event)
    {
        //fetch details of current event
        $event = Event::find($event);

        //thumbnail images
        $widget = Event::all()->get(6);
        return view('pages.event-details')
            ->withEvent($event)
            ->withWidgets($widget);
    }

    public function event_register_create($eventName)
    {
        $titles = Title::all();

        $titlesArray = array();

        foreach ($titles as $title) {
            $titlesArray[$title->title] = $title->title;
        }

        $event = Event::where('name', $eventName)
            ->where('registration_ongoing', 1)
            ->first();

        if ($event) {
            return view('pages.event_register')
                ->withEvent($event)
                ->withTitles($titlesArray);
        }

        return redirect()->route('pages.index');
    }

    public function event_register_store(Request $request)
    {
        $this->validate($request, array(
            'title' => 'string|required|max:255',
            'last_name' => 'string|required|max:255',
            'other_names' => 'string|required|max:255',
            'institution' => 'string|required|max:255',
            'email' => 'string|required|max:255',
            'telephone_1' => 'string|required|max:255',
            'telephone_2' => 'nullable|string|max:255',
            'address' => 'required|string|max:255',
            'name_order' => 'required|string|max:255',
            'status' => 'required|string|max:255',
            'accommodation' => 'required|string|max:255',
            'num_of_days_staying' => 'required|string|max:255',
            'registration_code' => 'string|required|max:255',
        ));

        // check if code is valid and assigned
        $checkCode = RegistrationCode::where('code', $request->registration_code)
            ->where('phone', $request->telephone_1)
            ->orWhere('phone', $request->telephone_2)
            ->first();

        if (collect($checkCode)->isEmpty()) {
            Session::flash('error', 'Phone number and registration code do not match');

            return back();
        }

        // check if the code is already in use

        $checkIfCodeUsed = EventRegister::where('registration_code', $request->registration_code)
            ->first();

        if (!collect($checkIfCodeUsed)->isEmpty()) {
            Session::flash('error', 'Code has been used already');

            return back();
        }

        $event = Event::find($request->event_id);

        $eventRegister = new EventRegister;

        $eventRegister->title = $request->title;
        $eventRegister->last_name = $request->last_name;
        $eventRegister->other_names = $request->other_names;
        $eventRegister->institution = $request->institution;
        $eventRegister->email = $request->email;
        $eventRegister->telephone_1 = $request->telephone_1;
        $eventRegister->telephone_2 = $request->telephone_2;
        $eventRegister->address = $request->address;
        $eventRegister->name_order = $request->name_order;
        $eventRegister->status = $request->status;
        $eventRegister->accommodation = $request->accommodation;
        $eventRegister->num_of_days_staying = $request->num_of_days_staying;
        $eventRegister->registration_code = $request->registration_code;
        $eventRegister->event_id = $event->id;

        $eventRegister->save();

        Session::flash('success', 'Registration successful');

        return redirect()->route('pages.event-details', $event->id);

    }

    // -------------------------------------------------------training and mentorship---------------------------------------------------------------

    public function training_mentorship()
    {

        $programmes = TrainingMentorship::all();

        return view('pages.training_mentorship')
            ->withProgrammes($programmes);
    }
    
    public function training_mentorship_single($programme)
    {

        $programme = TrainingMentorship::find($programme);
        return view('pages.training_mentorship_single')
            ->withProgramme($programme);
    }
    // --------------------------------------------------------------sponsors page--------------------------------------------------------------
    public function sponsors()
    {

        $sponsors = Sponsor::all();

        return view('pages.sponsors')
            ->withSponsors($sponsors);
    }

    //search result
    public function search(Request $request)
    {

        $request->validate([
            'query' => 'required|min:3',
        ]);

        $query = $request->input('query');
        $publication = Publication::where('title', 'like',"%$query%")
                                ->orWhere('author', 'like',"%$query%")
                                ->get();

        return view('pages.search')
            ->withPublication($publication);
    }
}
