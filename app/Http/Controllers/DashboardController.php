<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Gallery;
use App\Publication;
use App\AssocSchool;
use App\Event;
use App\TrainingMentorship;
use App\Slider;
use App\Sponsor;
use App\Contact;

class DashboardController extends Controller
{
    //
    // public function __construct()
    // {
    //     $this->middleware(['admin']);
        
    // }

    public function index(){
        //do all the stas details here
        $eventCount = Event::all()->count();
        $galleryCount = Gallery::all()->count();
        $publicationCount = Publication::all()->count();
        $messageCount = Contact::all()->count();
        $schoolCount = AssocSchool::all()->count();
        $trainingCount = TrainingMentorship::all()->count();
        $sliderCount = Slider::all()->count();
        $sponsorCount = Sponsor::all()->count();



        return view('dashboard.index')
            ->withEvent($eventCount)
            ->withGallery($galleryCount)
            ->withContact($messageCount)
            ->withPublication($publicationCount)
            ->withSchool($schoolCount)
            ->withTraining($trainingCount)
            ->withSlider($sliderCount)
            ->withSponsor($sponsorCount);
    }
}
