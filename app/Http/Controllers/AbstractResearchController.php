<?php

namespace App\Http\Controllers;

use App\AbstractResearch;
use Illuminate\Http\Request;
use Session;
class AbstractResearchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function __construct()
    // {
    //     $this->middleware(['admin']);
        
    // }
    
    public function index()
    {
        $docs = AbstractResearch::all();
        return view('abstractResearch.index')->withDocs($docs);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('abstractResearch.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, array(
            'conference_year' => 'required|string',
            'document' => 'required|mimes:docs,pdf,docx',
            'publication_type' => 'required',
        ));



        $doc = new AbstractResearch();
        $doc->conference_year = $request->conference_year;
        $doc->publication_type = $request->publication_type;


        if($request->hasFile('document')){

            $filename =$request->document->getClientOriginalName();
            $request->document->storeAs('public/abstractResearch', $filename);

           
             $doc->document = $filename;
             $doc->save();
             
        }

        Session::Flash('success','Added Successfully');
        return redirect()->route('abstractresearch.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $doc = AbstractResearch::find($id);
        $doc->delete();
        Session::flash('success', 'Deleted Successfully');
        return redirect()->route('abstractresearch.index');
    }
}
