<?php

namespace App\Http\Controllers;

use App\Event;
use App\Title;
use Illuminate\Http\Request;
use Image;
use Session;
use Purifier;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    // public function __construct()
    // {
    //     $this->middleware(['admin']);
        
    // }
    
    public function index()
    {
        //fetch and populate title in dropdown
        $events = Event::Paginate(10);

        return view('event.index')
            ->withEvents($events);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $titles = Title::all();

        //create ann array to hold titles
        $title_arr = [];

        foreach ($titles as $title) {
            $title_arr[$title->id] = $title->title;
        }
        return view('event.create')
            ->withTitles($title_arr);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, array(
            'name' => 'string|required',
            'title_id' => 'numeric|required',
            'host' => 'string|required',
            'event_date' => 'date|required',
            'event_venue' => 'required|string',
            'content' => 'required|string',
            'photo' => 'required|mimes:jpeg,png,jpg',
        ));

        $event = new Event;

        $event->name = strtoupper($request->name);
        $event->title_id = $request->title_id;
        $event->event_date = $request->event_date;
        $event->event_venue = $request->event_venue;
        $event->host = $request->host;
        $event->content = Purifier::clean($request->content);

        if ($request->hasFile('photo')) {
            // get the image
            $image = $request->file('photo');
            //create a file name using time and append image ext
            $filename = time() . '.' . $image->getClientOriginalExtension();
            // set the location for the file
            $location = public_path('images/event/' . $filename);
            // resize maintaining aspect ratio and saving to location
            // Image::make($image)->resize(0, 0, function($constraint){
            //     $constraint->aspectRatio();
            // })->resizeCanvas(10, 10, 'center')->save($location);

            Image::make($image)->fit(800, 600, function ($constraint) {
                $constraint->upsize();
            })->save($location);
            //set the value of wings in the db to file name
            $event->photo = $filename;
        }

        $event->save();

        Session::flash('success', 'Added Successfully');
        return redirect()->route('event.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $event = Event::find($id);

        $titles = Title::all();

        //create ann array to hold titles
        $title_arr = [];

        foreach ($titles as $title) {
            $title_arr[$title->id] = $title->title;
        }

        return view('event.show')
            ->withEvent($event)
            ->withTitles($title_arr);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Event $event)
    {
        //get all titles
        $titles = Title::all();

        $title_arr = [];

        foreach ($titles as $title) {
            $title_arr[$title->id] = $title->title;
        }
        return view('event.edit')->withEvent($event)->withTitles($title_arr);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, array(
            'name' => 'string|required',
            'title_id' => 'numeric|required',
            'host' => 'string|required',
            'event_date' => 'date|required',
            'event_venue' => 'required|string',
            'content' => 'required|string',
            'photo' => 'sometimes|required|mimes:jpeg,png,jpg',
        ));
        // return $request;
        $event = Event::find($id);

        $event->name = strtoupper($request->name);
        $event->title_id = $request->title_id;
        $event->event_date = $request->event_date;
        $event->event_venue = $request->event_venue;
        $event->host = $request->host;
        $event->registration_ongoing = $request->registration_ongoing;
        $event->content = Purifier::clean($request->content);

        if ($request->hasFile('photo')) {
            // get the image
            $image = $request->file('photo');
            //create a file name using time and append image ext
            $filename = time() . '.' . $image->getClientOriginalExtension();
            // set the location for the file
            $location = public_path('images/event/' . $filename);
            // resize maintaining aspect ratio and saving to location
            // Image::make($image)->resize(0, 0, function($constraint){
            //     $constraint->aspectRatio();
            // })->resizeCanvas(10, 10, 'center')->save($location);

            Image::make($image)->fit(800, 600, function ($constraint) {
                $constraint->upsize();
            })->save($location);
            //set the value of wings in the db to file name
            $event->photo = $filename;
        }

        $event->save();

        Session::flash('success', 'Updated Successfully');
        return redirect()->route('event.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $event = Event::find($id);
        $event->delete();

        return redirect()->route('event.index');
    }
}
