<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Image;
use Session;
use App\TrainingMentorship;
class TrainingMentorshipController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function __construct()
    // {
    //     $this->middleware(['admin']);
        
    // }

    
    public function index()
    {
        $training = TrainingMentorship::paginate(10);
        return view('training_mentorship.index')
                    ->withProgrammes($training);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('training_mentorship.create');
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request;
        $this->validate($request, array(
            'name'     =>  'required|string',
            'photo'    =>  'required|mimes:png,jpg,jpeg',
            'content'  =>  'required|string',
        ));

        $training = new TrainingMentorship;
        $training->name     =  $request->name;
        $training->photo    =  $request->photo;
        $training->content  =  $request->content;

        // use image processor here
        if ($request->hasFile('photo')) {
            // get the image
            $image = $request->file('photo');
            //create a file name using time and append image ext
            $filename = time() . '.' . $image->getClientOriginalExtension();
            // set the location for the file
            $location = public_path('images/training/' . $filename);
            // resize maintaining aspect ratio and saving to location
            Image::make($image)->fit(800, 600, function ($constraint) {
                $constraint->upsize();
            })->save($location);
            //set the value of wings in the db to file name
            $training->photo = $filename;
        }
        

        $training->save();

        Session::flash('success',$request->name.'Added successfully');
        return redirect()->route('training_mentorship.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $training = TrainingMentorship::find($id);

        return view('training_mentorship.show')
                ->withTraining($training);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $training = TrainingMentorship::find($id);

        return view('training_mentorship.edit')
                ->withTraining($training);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // return $request;
        $this->validate($request, array(
            'name'     =>  'required|string',
            'photo'    =>  'required|mimes:png,jpg,jpeg',
            'content'  =>  'required|string',
        ));

        $training =  TrainingMentorship::find($id);
        $training->name     =  $request->name;
        $training->photo    =  $request->photo;
        $training->content  =  $request->content;

        // use image processor here
        if ($request->hasFile('photo')) {
            // get the image
            $image = $request->file('photo');
            //create a file name using time and append image ext
            $filename = time() . '.' . $image->getClientOriginalExtension();
            // set the location for the file
            $location = public_path('images/training/' . $filename);
            // resize maintaining aspect ratio and saving to location
            Image::make($image)->fit(800, 600, function ($constraint) {
                $constraint->upsize();
            })->save($location);
            //set the value of wings in the db to file name
            $training->photo = $filename;
        }
        

        $training->save();

        Session::flash('success',$request->name.'Added successfully');
        return redirect()->route('training_mentorship.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $training = TrainingMentorship::find($id);
        $training->delete();
        return redirect()->route('training_mentorship.index');
    }
}
