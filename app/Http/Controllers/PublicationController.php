<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Publication;
use Session;
Use ImageProccessor;

use App\PublicationCategory;
class PublicationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function __construct()
    // {
    //     $this->middleware(['admin']);
        
    // }
    public function index()
    {
        //fetch associate schools
        // $schools = AssocSchool::all();

        //fetch all publications categories
        $categories = PublicationCategory::all();

        //create array
        // $schools_arr = [];
        $categories_arr =[];

        // foreach($schools as $school){
        //     $schools_arr[$school->id] = $school->name;
        // }
        foreach($categories as $category){
            $categories_arr[$category->id] = $category->name;
        }

        $publication = Publication::paginate(10);
        return view('publication.index')
                ->withPublications($publication)
                ->withCategories($categories_arr);
                // ->withSchools($schools_arr);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //fetch all associate schools
        // $schools = AssocSchool::all();

        //fetch all publications categories
        $categories = PublicationCategory::all();

        //create array
        $schools_arr = [];
        $categories_arr =[];

        
        foreach($categories as $category){
            $categories_arr[$category->id] = $category->name;
        }
        return view('publication.create')
                ->withCategories($categories_arr);
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $this->validate($request, array(
            'pub_title' => 'required|string',
            'author' => 'required|string',
            'document' => 'required|mimes:doc,pdf,docx',
            'publication_category_id' => 'required|numeric',
            'publication_date' => 'required',
            
        ));

        // return $request;
        $publication = new Publication;

        $publication->title                              = $request->pub_title;
        $publication->author                             = $request->author;
        $publication->publication_category_id            = $request->publication_category_id;
        $publication->document                           = $request->document;
        $publication->publication_date                   = $request->publication_date;   

        if($request->hasFile('document')){

            $filename =$request->document->getClientOriginalName();
            $request->document->storeAs('public/publication', $filename);

           
             $publication->document = $filename;
             $publication->save();
             
        }
        


        Session::flash('success','Added successfully');        
        return Redirect()->route('publication.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       
        $publication = Publication::find($id);

        $categories = PublicationCategory::all();
        //create array
        $categories_arr =[];

        
        foreach($categories as $category){
            $categories_arr[$category->id] = $category->name;
        }
        
        return view('publication.edit')
                    ->withPublication($publication)
                    ->withCategories($categories_arr);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, array(
            'title' => 'required|string',
            'author' => 'required|string',
            'document' => 'required|mimes:doc,pdf,docx',
            'publication_category_id' => 'required|numeric',
            'publication_date' => 'required',
            
        ));

        // return $request;
        $publication = Publication::find($id);

        $publication->title                              = $request->title;
        $publication->author                             = $request->author;
        $publication->publication_category_id            = $request->publication_category_id;
        $publication->document                           = $request->document;
        $publication->publication_date                   = $request->publication_date;   


        if($request->hasFile('document')){

            $filename =$request->document->getClientOriginalName();
            $request->document->storeAs('public/publication', $filename);

           
             $publication->document = $filename;
             
        }
        


        $publication->save();
        Session::flash('success','Updated successfully');        
        return Redirect()->route('publication.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $publication = Publication::find($id);
        $publication->delete();
         
        Session::flash('success','Removed Successfully');
        return redirect()->route('publication.index');
    }
}
