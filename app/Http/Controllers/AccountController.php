<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Session;

// use App\Account;
class AccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $accounts = User::all();
        return view('account.index')->withAccounts($accounts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('account.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, array(
            'name' => 'required|string',
            'email' => 'required|email|unique:users',
            'name' => 'required|string',
            'password' => 'required',
        ));

        $account = new User;
        $account->name = $request->name;
        $account->email = $request->email;
        $account->password = md5($request->password);
        $account->is_admin = 0;

        $account->save();

        Session::flash('Success', 'Added Successfully');
        return redirect()->route('account.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $account = User::find($id);
        return view('account.edit')
            ->withAccount($account);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, array(
            'name' => 'required|string',
            'email' => 'required|email|unique:users',
            'name' => 'required|string',
            'password' => 'required',
        ));

        $account = User::find($id);
        $account->name = $request->name;
        $account->email = $request->email;
        $account->password = md5($request->password);
        $account->is_admin = 0;

        $account->save();

        Session::flash('success', 'Updated Successfully');
        return redirect()->route('account.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $account = User::find($id);
        $account->delete();

        Session::flash('success', 'Deleted Successfully');
        return redirect()->route('account.index');
    }
}
