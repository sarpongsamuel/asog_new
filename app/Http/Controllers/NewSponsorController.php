<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\NewSponsor;
Use Session;
class NewSponsorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    // public function __construct()
    // {
    //     $this->middleware(['admin']);
        
    // }
    
    public function index()
    {
        $new = NewSponsor::all();
        return view('newSponsor.index')->withNew($new);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $this->validate($request, array(
            'fullname'      => 'string|required',
            'company'       => 'string|required',
            'email'         => 'email|required',
            'telephone'     => 'numeric|required|min:10',
            'message'       => 'required|string',
           
        ));
        

        $newSponsor = new NewSponsor;

        $newSponsor->fullname       = $request->fullname;
        $newSponsor->company        = $request->company;
        $newSponsor->email          = $request->email;
        $newSponsor->telephone      = $request->telephone;
        $newSponsor->message        = $request->message;
        $newSponsor->save();
        
        
        Session::flash('success', 'Thank You. Wel Will Contact You Shortly');
        return redirect()->route('pages.sponsors');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $new  = NewSponsor::find($id);
        return view('newsponsor.show')->withNew($new);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $new  = NewSponsor::find($id);
        $new->delete();
        Session::flash('success','Deleted Successfully');
        return redirect()->route('newsponsor.index');
    }
}
