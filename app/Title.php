<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Title extends Model
{
    public function events(){
        return $this->hasMany('App\Event');
    }
}
