<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Publication extends Model
{
    public function publicationCategory(){
        return $this->belongsTo('App\PublicationCategory');
    }

    // public function associateSchool(){
    //     return $this->belongsTo('App\AssocSchool');
    // }
}
