<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    public function title(){
        return $this->belongsTo('App\Title');
    }
}
